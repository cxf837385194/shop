//存入用户信息
function saveLoginUser(user) {
    let loginUserJson = JSON.stringify(user);
    localStorage.setItem("login_user", loginUserJson);
}

//取出用户信息
function getLoginUser() {
    let sessionUserJson = localStorage.getItem("login_user");
    let sessionUser = JSON.parse(sessionUserJson);
    return sessionUser;
}