//商品分类模块类别 js工具


//存入根据类别Tid查询到的商品类别信息
function saveGoodsType(type) {
    let typeGoodsJson = JSON.stringify(type);
    localStorage.setItem("tId_type", typeGoodsJson);
}

//取出商品类别信息
function getGoodsType() {
    let sessionGoodsTypeJson = localStorage.getItem("tId_type");
    let type = JSON.parse(sessionGoodsTypeJson);
    return type;
}


function typeShow() {
    $.ajax({
        url: '/shop-api/type/findAll', //调用后端查询所有类别的接口
        type: 'get',                   //请求方式
        dataType: 'json',
        success: function (data) {
            console.log(data);
            console.log('开始展示所有商品类别信息！！！');
            //     <!--     展示所有商品类别的模板 start       -->
            // <ul class="nav nav-pills">
            //     <li role="presentation" class="active"><a href="#">商品类别</a></li>
            //     <li role="presentation"><a href="#">商品类别</a></li>
            //     <li role="presentation"><a href="#">商品类别</a></li>
            //     </ul>
            //     <!--     展示所有商品类别的模板 end       -->
            $.each(data.data, function (index, type) {
                let tr;

                tr = '<li role="presentation" onmouseover="showColor();"  onmouseout ="removeColor()">'+   //li标签绑定一个鼠标移入移出事件（改变class="active"）//变换颜色
                     '<a href="javascript:void(0);" onclick="toGoodsList('+type.tId+')">'+type.tName+'</a>'+  //a标签绑定一个鼠标点击事件（重定向至该种类别的商品展示页面）
                     '</li>';
                $("#show_allGoodsType").append( tr );
            })
        }
    })
};


//点击链接跳转至分类商品页面

function toGoodsList(tId) {
    //打印获取到的商品类别TID
    console.log(tId);
    //发起Ajax请求，请求参数:tId
    $.ajax({
            url:'/shop-api/type/findTypeByTid', //调用后端通过类别Tid查询类别接口
            type:'post', //请求方式（前后端统一）!!!
            data:JSON.stringify({tId:tId}),
            contentType: "application/json",
            // ajax请求后端数据成功
            success:function(data){
                console.log(data); //控制台打印后端返回的数据
                console.log('将要跳转至商品分类页面');
                saveGoodsType(data); //将从后端获取数据存储起来
                //页面刷新
                //这边只要刷新显示商品的页面就行了
                $(window).attr('location', '/shop-app/goods/goodsList.html');

            }
        }
    )
}



//li标签  鼠标移入事件（class="active"）显示颜色
function showColor() {
    //控制台打印提示信息
    console.log('检测到鼠标移入，显示颜色！！！')
    $(this).addClass("active")
}

//li标签  鼠标移出事件 不显示颜色
function removeColor() {
    //控制台打印提示信息
    console.log('检测到鼠标移入，显示颜色！！！')
    $(this).removeClass();
}
