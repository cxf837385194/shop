//商品模块js工具



//存入根据商品ID查询到的商品信息
function saveGoods(goods) {
    let goodsJson = JSON.stringify(goods);
    localStorage.setItem("gId_goods", goodsJson);
}

//取出商品信息
function getGoods() {
    let sessionGoodsJson = localStorage.getItem("gId_goods");
    let goods = JSON.parse(sessionGoodsJson);
    return goods;
}


//
//展示所有商品的渲染工具
// function showAllGoods() {
//
//
//     $.ajax({
//         // url:'/shop-api/goods/listAll', //调用后端接口
//         url:'/shop-api/goods/listAll', //调用后端接口
//         type:'get',
//         dataType:'json',
//         success:function(data) {
//             console.log(data);
//             console.log('开始展示所有商品信息！！！');
//
//             //渲染模板
//             // <div class="col-sm-6 col-md-4">
//             //         <div class="thumbnail">
//             //         <img src="..." alt="...">
//             //         <div class="caption">
//             //         <h3>Thumbnail label</h3>
//             //     <p>...</p>
//             //     <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
//             //     </div>
//             //     </div>
//             //     </div>
//
//                 $.each(data.data, function (index, goods) {
//                     let tr;
//                     // tr = '<input type="hidden" value="'+goods.gId+'">'+
//                     //     '<img src="./img/'+goods.gImage+'" onclick="toGoodsDetails('+goods.gId+')" >'+
//                     //     ' <div class="caption">'+
//                     //     '<h3>'+
//                     //     '<small>'+
//                     //     '商品名称：'+
//                     //     goods.gName+
//                     //     '</small>'+
//                     //     '</h3>'+
//                     //     '</div>';
//
//                     tr = '<div class="thumbnail">' +
//                         '<img src="/shop-app/goods/img/' + goods.gImage + '" onclick="toGoodsDetails(' + goods.gId + ')" width=350px height=250px>' + //商品图片
//                         '<div class="caption">' +
//                         '<h3>' + '商品名称：' + goods.gName + '</h3>' + //商品的名称
//                         '<p>' + goods.gInfo + '</p>' +   //商品描述
//                         '<p>' +
//                         '<button class="btn btn-warning" onclick="toCart(' + goods.gId + ')">加入购物车' +  //注意绑定点击事件
//                         '<span class="glyphicon glyphicon-shopping-cart">' +
//                         '</button>' +
//                         '&nbsp;&nbsp;&nbsp;&nbsp;' +
//                         '<button class=" btn btn-danger" onclick="">直接购买</button></p>' +  //注意绑定点击事件
//                         '</div>' +
//                         '</div>';
//                     $("#show_allGoods").append('<div class="col-sm-6 col-md-4">' + tr + '</div>')
//                 })
//         }
//     })};


//通过Tid查询展示所有满足条件商品的渲染工具
function showAllGoods() {

    let goodsTypeList = getGoodsType();  //取出商品类别信息
    goodsTypeList = JSON.parse(goodsTypeList);//将json字符串转换成json对象
    console.log(goodsTypeList); //前端控制台打印
    $.ajax({
        url:'/shop-api/goods/findGoodsByTid', //调用后端通过商品类别Tid查询商品信息接口
        type:'post',
        data:JSON.stringify({tId:goodsTypeList.data.tId}),
        dataType:'json',
        contentType: "application/json",
        success:function(data) {
            console.log(data);
            console.log('开始展示所有商品信息！！！');

            //渲染模板
            // <div class="col-sm-6 col-md-4">
            //         <div class="thumbnail">
            //         <img src="..." alt="...">
            //         <div class="caption">
            //         <h3>Thumbnail label</h3>
            //     <p>...</p>
            //     <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
            //     </div>
            //     </div>
            //     </div>
            // let goodsTypeList = getGoodsType();
            // console.log(goodsTypeList);
            // goodsTypeList = JSON.parse(goodsTypeList);//将json字符串转换成json对象
            // console.log(goodsTypeList); //前端控制台打印
            //
            // console.log(goodsTypeList.data.tId);
            //
            // console.log(data.data.tId)

            $.each(data.data, function (index, goods) {

               let uId = getLocalStorage("loginUser").uId;
                let tr;
                // tr = '<input type="hidden" value="'+goods.gId+'">'+
                //     '<img src="./img/'+goods.gImage+'" onclick="toGoodsDetails('+goods.gId+')" >'+
                //     ' <div class="caption">'+
                //     '<h3>'+
                //     '<small>'+
                //     '商品名称：'+
                //     goods.gName+
                //     '</small>'+
                //     '</h3>'+
                //     '</div>';

                tr = '<div class="thumbnail">' +
                    '<img src="/shop-app/goods/img/' + goods.gImage + '" onclick="toGoodsDetails(' + goods.gId + ')" width=350px height=250px>' + //商品图片
                    '<div class="caption">' +
                    '<h3>' + '商品名称：' + goods.gName + '</h3>' + //商品的名称
                    '<h4 style="color: darkred">' + '￥' + goods.gPrice + '</h4>' + //商品的价格
                    '<p>' + goods.gInfo + '</p>' +   //商品描述
                    '<p>' +
                    '<button class="btn btn-warning" onclick="toCart(' + goods.gId + ')">加入购物车' +  //注意绑定点击事件
                    '<span class="glyphicon glyphicon-shopping-cart">' +
                    '</button>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;' +
                    '<button class=" btn btn-danger" onclick="toCart(' + goods.gId + ')">直接购买</button></p>' +  //注意绑定点击事件
                    '</div>' +
                    '</div>';
                $("#show_allGoods").append('<div class="col-sm-6 col-md-4">' + tr + '</div>')
            })

        }
    })};

//点击图片跳转至商品详情页面
function toGoodsDetails(gId) {
    //打印获取到的商品ID
    console.log(gId);
    //发起Ajax请求，请求参数:gId
    $.ajax({
            url:'/shop-api/goods/findGoodsByGid', //后端接口
            type:'post', //请求方式（前后端统一）!!!
            data:JSON.stringify({gId:gId}),
             contentType: "application/json",
            // ajax请求后端数据成功
            success:function(data){
                console.log(data); //控制台打印后端返回的数据
                console.log('将要跳转至商品详情页面');
                saveGoods(data); //将从后端获取数据存储起来
                // 页面跳转
                // $.attr("src","/shop-app/goodsDetail.html");
                //页面重定向
                $(window).attr('location', '/shop-app/goods/goodsDetail.html');


            }
        }
    )
}


//点击加入购物车跳转至购物车页面
function toCart(gId) {
    //打印获取到的商品ID
    console.log(gId);
    //发起Ajax请求，请求参数:gId
    $.ajax({
            url:'/shop-api/goods/findGoodsByGid', //后端查询商品ID接口
            type:'post', //请求方式（前后端统一）!!!
            data:JSON.stringify({gId:gId}),
            contentType: "application/json",
            // ajax请求后端数据成功
            success:function(data){
                console.log(data); //控制台打印后端返回的数据
                console.log('将要跳转至购物车页面');
                saveGoods(data); //将从后端获取数据存储起来
                var loginUser= getLocalStorage("loginUser");
                console.log(loginUser);
                //发起ajax请求 请求cart后端接口 增加
                $.ajax({
                    url:'/shop-api/cart/selectCartByCid', //后端接口
                    type:'get', //请求方式（前后端统一）!!!
                    data:({
                        gId:gId,
                        uId:loginUser.uId
                    }),
                    contentType: "application/json",
                    // ajax请求后端数据成功
                    success: function (data) {
                        // 页面跳转
                        // $(window).attr('location', '/shop-app/cart/cart.html');
                        window.parent.location.href = '/shop-app/cart/cart.html';
                    }
                })


            }
        }
    )
}



//商品详情页面渲染工具
function GoodsDetails() {
    //获取另一个页面存储的后端数据
    let goodsList = getGoods();
    goodsList = JSON.parse(goodsList);//将json字符串转换成json对象
    console.log(goodsList); //前端控制台打印
    let tr ;
    tr = '<div class="col-xs-6 col-md-6">'+
        ' <a href="#" class="thumbnail">'+
        ' <img  src="/shop-app/goods/img/'+goodsList.data.gImage+'" width="560px" height="560px"  >'+
        '</a>'+
        '</div>'+
        ' <div class="col-xs-6 col-md-6">'+
        ' <div class="panel panel-default" style="height: 560px">'+
        '<div class="panel-heading">'+
        '商品详情：'+
        '</div>'+
        '<div class="panel-body">'+
        '<h3>产品名称:'+goodsList.data.gName+'</h3>'+
        ' <div style="margin-left: 10px;">'+
        ' <p>'+  ' 市场价格:'+
        ' <span class="text-danger" style="font-size: 15px;">'+goodsList.data.gPrice+'</span>'+
        '<span class="glyphicon glyphicon-yen">'+ '</span> '+
        '</p> '+
        '<p>'+'上市时间:'+formatDate(goodsList.data.gTime)+'</p>'+
        '<p> '+ '热销指数:'+goodsList.data.gState+'</p>'+
        '<p> '+ '详细介绍:'+ '</p>'+
        '<p>'+  goodsList.data.gInfo+  '</p>'+
        '<a href="#" class="btn btn-warning" onclick="toCart('+goodsList.data.gId+')">'+ '加入购物车'+
        '<span class="glyphicon glyphicon-shopping-cart">'+  '</span>'+ '</a>'+
        '&nbsp;&nbsp;&nbsp;&nbsp;'+
        ' <button class="btn btn-danger" onclick="toCart('+goodsList.data.gId+')">'+ '直接购买'+ '</button>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';
    $("#show_goodsDet").append('<div>'+tr+'</div>')
}


//直接购买按钮 绑定点击事件（传递参数：1.对应的订单编号（唯一的！！！） 2.商品的总金额）
function payMsg(oId,Count) {
    //打印获取到的参数
    console.log('点击商品直接购买按钮获取的订单编号(oId):');
    console.log(oId);
    console.log('点击商品直接购买按钮获取的商品的总金额(Count):');
    console.log(Count);

    //发起Ajax请求（将获取到的参数传递给后端）

}






//js日期转换工具
//传入参数为时间戳
function formatDate(timestamp){
    let date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    let Y = date.getFullYear() + '-';
    let M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    let D = (date.getDate() < 10 ? '0'+date.getDate() : date.getDate()) + ' ';
    let h = (date.getHours() < 10 ? '0'+date.getHours() : date.getHours()) + ':';
    let m = (date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()) + ':';
    let s = (date.getSeconds() < 10 ? '0'+date.getSeconds() : date.getSeconds());

    let strDate = Y+M+D+h+m+s;
    return strDate;
}