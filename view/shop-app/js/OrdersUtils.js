//订单模块js工具

//存入支付宝页面html字符串
function setPayHtml(payHtml) {
    localStorage.setItem("payHtml", payHtml);
}

function getPayHtml() {
    let payHtml = localStorage.getItem("payHtml");
    return payHtml;
}




//存入根据订单编号查询到的订单信息
function saveOrder(order) {
    let orderJson = JSON.stringify(order);
    localStorage.setItem("order", orderJson);
}

//取出订单信息
function getOrder() {
    let sessionorderJson = localStorage.getItem("order");
    let order = JSON.parse(sessionorderJson);
    return order;
}

//存入订单编号
function saveOid(oId) {
    localStorage.setItem("oId", oId);
}

//取出订单编号
function getOid() {
    let oId = localStorage.getItem("oId");
    return oId;
}

//存入支付相关的参数
function setPayParams(payParams) {
    let payParamsJson = JSON.stringify(payParams);
    localStorage.setItem("payParams", payParamsJson);
}

//取出支付相关的参数
function getPayParams() {
    let sessionPayParamsJson = localStorage.getItem("payParams");
    let payParams = JSON.parse(sessionPayParamsJson);
    return payParams;
}


// <!-- 所有订单数据渲染工具  -->
function drawingParams() {
    let orderList = getOrder();
    orderList = JSON.parse(orderList);//将json字符串转换成json对象
    console.log(orderList); //前端控制台打印
    //进行数据展示
    $.each(orderList.data, function (index, order) {
        let tr;
        tr = '<th >' + order.oId + '</th>' +
            '<th>' + '￥'+order.oCount + '</th>' +
            '<th>' + orderState(order.oState) + '</th>' +   //使用js动态判断订单状态 状态码：0(未支付) 1(已经付款未发货) 2(发货待收货) 3 (收货待评价) 4(订单完成) 5( 退货状态)
            '<th>' + formatDate(order.oCreateTime) + '</th>' +   //调用js日期转换工具，对后端返回的时间戳进行格式化
            '<th>' + formatDate(order.oUpdateTime) + '</th>' +   //调用js日期转换工具，对后端返回的时间戳进行格式化
            '<th>' + order.address.aDetail + '</th>' +   //注意关联地址模块获取详细地址信息
            '<th>' +
            '<button type="button" class="btn btn-danger btn-sm" onclick="toDetails(" + orders.oId + ")" >' + '订单详情' + '</button>' +   //绑定单击事件（获取uId），跳转至订单详细信息页面
            '&nbsp;&nbsp;&nbsp;&nbsp;'+
            buttonWord(order)
            + '</th>';
        // <button type="button" class="btn btn-danger btn-sm" onclick="showOrder('${order.oid}')">订单详情</button>
        $("#tb_list").append('<tr>' + tr + '</tr>')
    })
};

//按订单编号页面，渲染工具
function drawingByOid() {
    let orders = getOrder();
    orders = JSON.parse(orders);//将json字符串转换成json对象
    console.log(orders); //前端控制台打印
    //进行数据展示
    let tr;
    tr = '<th >' + orders.oId + '</th>' +
        '<th>' + '￥'+orders.oCount + '</th>' +
        '<th>' + orderState(orders.oState) + '</th>' +   //使用js动态判断订单状态 状态码：0(未支付) 1(已经付款未发货) 2(发货待收货) 3 (收货待评价) 4(订单完成) 5( 退货状态)
        '<th>' + formatDate(orders.oCreateTime) + '</th>' +   //调用js日期转换工具，对后端返回的时间戳进行格式化
        '<th>' + formatDate(orders.oUpdateTime) + '</th>' +   //调用js日期转换工具，对后端返回的时间戳进行格式化
        '<th>' + orders.address.aDetail + '</th>' +   //注意关联地址模块获取详细地址信息
        '<th>' +
        '<button type="button" class="btn btn-danger btn-sm" onclick="toDetails(' + orders.oId + ')" >' + '订单详情' + '</button>' +   //绑定单击事件（获取uId），跳转至订单详细信息页面
        '&nbsp;&nbsp;&nbsp;&nbsp;'+
        buttonWord(orders) +
        '</th>';
    // <button type="button" class="btn btn-danger btn-sm" onclick="showOrder('${order.oid}')">订单详情</button>
    $("#tb_list").append('<tr>' + tr + '</tr>')
};


function drawing() {
    //获取用户ID
    console.log("获取到的用户ID：");
    let uId = getLocalStorage("loginUser").uId;
    $.ajax({
        url: '/shop-api/orders/showAllOrders', //调用后端接口
        type: 'post',
        data:JSON.stringify({
            uId:uId
        }),
        dataType: 'json',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            $.each(data.data, function (index, orders) {
                let tr;
                tr = '<th >' + orders.oId + '</th>' +
                    '<th>' + '￥'+orders.oCount + '</th>' +
                    '<th>' + orderState(orders.oState) + '</th>' +   //使用js动态判断订单状态 状态码：0(未支付) 1(已经付款未发货) 2(发货待收货) 3 (收货待评价) 4(订单完成) 5( 退货状态)
                    '<th>' + formatDate(orders.oCreateTime) + '</th>' +   //调用js日期转换工具，对后端返回的时间戳进行格式化
                    '<th>' + formatDate(orders.oUpdateTime) + '</th>' +   //调用js日期转换工具，对后端返回的时间戳进行格式化
                    '<th>' + orders.address.aDetail + '</th>' +   //注意关联地址模块获取详细地址信息
                    '<th>' +
                    '<button type="button" class="btn btn-danger btn-sm" onclick="toDetails(' + orders.oId + ')" >' + '订单详情' + '</button>' +   //绑定单击事件（获取uId），跳转至订单详细信息页面
                    '&nbsp;&nbsp;&nbsp;&nbsp;'+
                    buttonWord(orders)
                    + '</th>';
                // <button type="button" class="btn btn-danger btn-sm" onclick="showOrder('${order.oid}')">订单详情</button>
                $("#tb_list").append('<tr>' + tr + '</tr>')
            })
        }
    })
};

//订单详情页面数据渲染模板

//订单详情页面替换字段
function drawingOrderDetails(order) {
    let showOrderDetailsHtml = $("#orderDetailsBox").html();
    let orderDetailsHtml = "";
    let tempOrderHtml;
    //字段替换
    tempOrderHtml = showOrderDetailsHtml.replace(/{order.oId}/g, order.oId) //订单编号
        .replace(/{order.oCreateTime}/g, formatDate(order.oCreateTime)) //订单生成时间
        .replace(/{order.address.aName}/g, order.address.aName) //收件人
        .replace(/{order.address.aPhone}/g, order.address.aTel) //联系电话
        .replace(/{order.address.aDetail}/g, order.address.aDetail)//送货地址
        .replace(/{order.oCount}/g, order.oCount)//总价
        .replace(/{order.oAssess}/g, haveAssess(order.oAssess))     //订单评价  动态非空判断
        .replace(/ForPay/g, buttonForPay(order)); //在线支付按钮
    // console.log(tempOrderHtml);
    orderDetailsHtml += tempOrderHtml;
    //拼接商品展示html
    //html 填充到页面当中
    $("#orderDetailsBox").html(orderDetailsHtml);
}

//判断是否有评价
function haveAssess(oAssess) {
    if (oAssess == null) {
        return ''
    }else{
        return oAssess
    }
}



function drawingCheckPay(payParams) {
    let showCheckPayHtml = $("#checkPay").html();
    let checkPayHtml = "";
    let tempOrderHtml;
    //字段替换
    tempOrderHtml = showCheckPayHtml.replace(/{order.oId}/g, payParams.oId) //订单编号
        .replace(/{orders.oCount}/g, payParams.oCount); //在线支付按钮
    // console.log(tempOrderHtml);
    checkPayHtml += tempOrderHtml;
    //拼接商品展示html
    //html 填充到页面当中
    $("#checkPay").html(checkPayHtml);
}





//数据渲染
//js动态判断订单状态，是否显示在线支付按钮(传入参数为订单状态)
function buttonForPay(order) {
    //进行渲染时获取到数据对象
    console.log(order);
    //进行动态判断
    if (order.oState == 0) {
        return '<button type="button" onclick="pay(' + order.oId + ',' + order.oCount + ')" class="btn btn-success btn-sm">在线支付</button>' //待付款
    } else if (order.oState == 1) {
        return ''  //已付款,待发货
    } else if (order.oState == 2) {
        return '' //已发货,待收货
    } else if (order.oState == 3) {
        return '' //已收货,待评价
    } else if (order.oState == 4) {
        return '' //订单完成
    } else if (order.oState == 5) {
        return '' //退货状态
    }
}

//订单详情页面，在线支付按钮绑定点击事件(传入参数  1.订单编号  2.总金额)
function pay(oId, oCount) {
    console.log("获取到的订单编号：");
    console.log(oId);
    console.log("获取到的总金额：");
    console.log(oCount);
    //存入支付参数对象中
    let payParams = {
        oId:oId,
        oCount:oCount
    };
    //调用存入方法
    setPayParams(payParams);
    //重定向至支付确认页面
    $(window).attr('location', '/shop-app/orders/checkPay.html');
}





//动态获取oState（订单状态）,通过js进行动态判断,返回按钮文本内容
function buttonWord(orders) {
    if (orders.oState == 0) {
        return '<button type="button" class="btn btn-primary btn-sm" onclick="pay(' + orders.oId + ',' + orders.oCount + ');">' + '<span>' + '前去付款' + '</span>' + '</button>'
    } else if (orders.oState == 1) {
        return ''
    } else if (orders.oState == 2) {
        return '<button type="button" class="btn btn-info btn-sm" onclick="resuleOrder('+orders.oId+')">' + '<span>' + '确认收货' + '</span>' + '</button>'
    } else if (orders.oState == 3) {
        return '<button type="button" class="btn btn-warning btn-sm" onclick="toOrderAssess('+ orders.oId +')">' + '<span>' + '追加评价' + '</span>' + '</button>'
    }else if (orders.oState == 4) {
        return ''
    }else if (orders.oState == 5) {
        return ''
    }
}

//确认收货
function resuleOrder(oId) {
    $.ajax({
        url:'/shop-api/orders/chageOstateFour', //后端查询商品ID接口
        type:'post', //请求方式（前后端统一）!!!
        data:JSON.stringify({oId:oId}),
        contentType: "application/json",
        // ajax请求后端数据成功
        success:function(){
            //重定向至首页
            $(window).attr('location', '/shop-app/orders/order.html');
        }
    })
}



//点击追加评价按钮，重定向至进行订单评价页面(传递订单id)
function toOrderAssess(oId) {
    //控制台打印获取参数的过程信息
    console.log('点击追加评价按钮，获取到的订单编号：');
    console.log(oId);
    //调用封装的js，存储订单编号oId
    saveOid(oId);
    //重定向至进行订单评价页面
    $(window).attr('location', '/shop-app/orders/GetOrderAssess.html');
    // $.attr('str','/shop-app/orders/orderAssess.html')

}




//js动态判断订单状态码，输出订单状态
function orderState(oState) {
    if (oState == 0) {
        return '<span style="color: red">' + '待付款' + '</span>'
    } else if (oState == 1) {
        return '<span style="color: #2aabd2">' + '已付款,待发货' + '</span>'
    } else if (oState == 2) {
        return '<span style="color: orange">' + '已发货,待收货' + '</span>'
    } else if (oState == 3) {
        return '<span style="color: #ff4280">' + '已收货,待评价' + '</span>'
    } else if (oState == 4) {
        return '<span style="color: #1e7e34">' + '订单完成' + '</span>'
    } else if (oState == 5) {
        return '<span style="color: #071C35;">' + '退货状态' + '</span>'
    }
}

//订单详情按钮(动态获取uId)
function toDetails(oId) {

    // let oId = $('#get_oId').val();
    console.log("获取到的用户ID：");
    let uId = getLocalStorage("loginUser").uId;
    //点击订单详情按钮获取的oId
    console.log(oId);
    //进行Ajax请求
    $.ajax({
        url: '/shop-api/orders/findOrderByOid',
        type: 'post',
        data: JSON.stringify({
            uId:uId,
            oId: oId}),
        contentType: "application/json",
        // ajax请求后端数据成功
        success: function (data) {
            console.log(data); //控制台打印后端返回的数据
            console.log('将要跳转至订单详情页面');
            saveOrder(data);
            // 页面跳转(重定向)
            $(window).attr('location', '/shop-app/orders/orderDetails.html');
        }
    })
}


//支付宝支付页面渲染工具
function drawingPayHtml(payHtml) {
    $('#showPay').append(payHtml);
}




//js日期转换工具
//传入参数为时间戳
function formatDate(timestamp) {
    let date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    let Y = date.getFullYear() + '-';
    let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    let D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
    let h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    let m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    let s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    let strDate = Y + M + D + h + m + s;
    return strDate;
}