//存入根据订单编号查询到的订单信息
function saveOrder(order) {
    let orderJson = JSON.stringify(order);
    localStorage.setItem("oId_order", orderJson);
}

//取出订单信息
function getOrder() {
    let sessionorderJson = localStorage.getItem("oId_order");
    let order = JSON.parse(sessionorderJson);
    return order;
}