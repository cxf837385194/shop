//地址模块js工具


//存入地址对象信息
function saveAddress(address) {
    let addressJson = JSON.stringify(address);
    localStorage.setItem("address", addressJson);
}

//取出地址对象信息
function getAddress() {
    let sessionAddressJson = localStorage.getItem("address");
    let address = JSON.parse(sessionAddressJson);
    return address;
}



//渲染所有地址信息

function showAllAddress() {
    //从LocalStorage获取用户ID
    let uId = getLocalStorage("loginUser").uId;
    //打印从前端获取的uId  用户ID
    console.log("获取到的用户ID:");
    console.log(uId)

    $.ajax({
        //这个地方要改**********************
        url: '/shop-api/address/infoPost', //调用后端接口  查询所有地址信息的方法
        type: 'post',
        data:JSON.stringify({uId:uId}),
        dataType: 'json',
        contentType: "application/json",
        success: function (addressList) {
            console.log(addressList.data);
            $.each(addressList.data, function (index,address) {
                let tr;
                tr = '<th>'+address.aName+'</th>'+
                    '<th>'+address.aDetail+'</th>'+
                    '<th>'+address.aTel+'</th>'+
                    '<th>'+
                    '<button type="button" class="btn btn-info btn-sm" onclick="updateAddress('+address.aId+')" >' + '修改' + '</button>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;'+
                    '<button type="button" class="btn btn-danger btn-sm" onclick="deleteAddress('+address.aId+','+address.uId+')" >' + '删除' + '</button>' +
                    '</th>'+
                    ' <th>'+
                    '<button type="button" class="btn btn-light btn-sm"  >' + '是否设为默认地址' + '</button>' +
                    '</th>';
                //渲染模板  start
                // <tr>
                //     <th>收货人</th>
                //     <th>详细地址</th>
                //     <th>电话/手机</th>
                //     <th>操作</th>
                //     <th></th>
                //     </tr>
                //渲染模板  end
                $("#showAddress").append('<tr>' + tr + '</tr>')
            })
        }
    })
}

function deleteAddress(aId,uId) {
    console.log('从前端获取的的地址ID:');
    console.log(aId);
    console.log(uId);
    $.ajax({
        url:'/shop-api/address/deleteAddress', //后端接口
        type:'post', //请求方式（前后端统一）!!!
        data:JSON.stringify({aId:aId}),
        contentType: "application/json",
        // ajax请求后端数据成功
        success:function() {

            console.log(uId);
            console.log('ajax请求后端数据成功');
            location.reload();
            // $.ajax({
            //     url: '/shop-api/address/infoPost', //后端接口
            //     type: 'post', //请求方式（前后端统一）!!!
            //     data: JSON.stringify({uId: uId}),
            //     contentType: "application/json",
            //     success: function (data) {
            //         window.parent.location.href = '/shop-app/address/address.html';
            //         // $(window).attr('location', '/shop-app/address/address.html');
            //     }
            // })
            // console.log(data); //控制台打印后端返回的数据
            // //将data转化为json
            // data = JSON.parse(data);
            //
            // //设置data中的aId
            // data.data.aId = aId;
            // data = JSON.stringify(data);
            //
            //
            // console.log('将要跳转至地址修改页面');
            // saveAddress(data); //将从后端获取数据存储起来

            //将参数传递给下个页面
            //重定向
            // $(window).attr('location', '/shop-app/address/address.html');
        }

    }
    )
}

function updateAddress(aId) {

    console.log('从前端获取的地址ID:');
    console.log(aId);
    $.ajax({
            url:'/shop-api/address/findAddressByAid', //后端接口
            type:'post', //请求方式（前后端统一）!!!
            data:JSON.stringify({aId:aId}),
            contentType: "application/json",
            // ajax请求后端数据成功
            success:function(data){
                console.log(data); //控制台打印后端返回的数据
                //将data转化为json
                data = JSON.parse(data);

                //设置data中的aId
                data.data.aId = aId;
                data = JSON.stringify(data);


                console.log('将要跳转至地址修改页面');
                saveAddress(data); //将从后端获取数据存储起来

                //将参数传递给下个页面
                //重定向
                $(window).attr('location', '/shop-app/address/setAddress.html');

            }
        }
    )


}




//修改地址信息页面  字段替换工具
function drawingSetAddress(address) {

    let setAddressHtml = $("#setAddressBox").html();
    let ordersetHtml = "";
    let tempOrderHtml;
    console.log("111111");
    console.log(address);
    //字段替换
    tempOrderHtml = setAddressHtml.replace(/{address.aName}/g, address.data.aName) //订单编号
        .replace(/{address.aDetail}/g, address.data.aDetail) //订单生成时间
        .replace(/{address.aTel}/g, address.data.aTel) //在线支付按钮
        .replace(/{address.aId}/g, address.data.aId)
    // console.log(tempOrderHtml);
    ordersetHtml += tempOrderHtml;
    //拼接商品展示html
    //html 填充到页面当中
    $("#setAddressBox").html(ordersetHtml);


    //点击提交,获取表单的参数

}