//渲染显示商品列表
function renderingGoodsList(cartsList){
    let sum = 0;
    $.each(cartsList, function (index, cart) {
        let tr;
        let num = index+1 ;
        console.log(cart);
        // console.log(typeof index);
        // console.log(typeof num);
        tr =
            // '<span id="cid" style="display: none">' + cart.cId + '</span>' +
            '<th >' + num + '</th>' +
            '<th>' + cart.goods.gName + '</th>' +
            '<th id="price">' + cart.goods.gPrice + '</th>' +
            '<th width="100px">'+
            '<div class="input-group">'+
            '<span class="input-group-btn">'+
            '<button style="display: none" id="cid" >'+ cart.cId +'</button>'+
            '<button class="btn btn-default" type="button" id="reduction">'+'-'+'</button>'+
            '</span>'+

            '<p type="text" class="form-control"  style="width:40px" id="cNum">'+ cart.cNum +
            '</p>'+
            '<span class="input-group-btn" id="addSpan">'+
            '<button class="btn btn-default" type="button" id="addCnum" >'+'+'+'</button>'+
            '</span>'+
            '</div>'+
            '</th>' +
            '<th id="all">' + '￥'+cart.cCount + '</th>'+
            '<th>' +  '<button type="button" class="btn btn-default" id="delete">'+ '删除'
            +'</button>' + '</th>';
        $("#div_carts_list").append('<tr class="tr">' + tr + '</tr>')
        sum += cart.cCount;

    });
    $("#s_sum").html(sum);
}

// 更新商品金额总合计
function renderingGoodsList1(cartsList){
    let sum = 0;
    $.each(cartsList, function (index, cart) {
        let tr;
        let num = index+1 ;
        // console.log(typeof index);
        // console.log(typeof num);
        tr =
            // '<th >' + '<span id="addaId" style="display: none">' + address.aId + '</span>' + '</th>' +
            '<th >' + num + '</th>' +
            '<th>' + cart.goods.gName + '</th>' +
            '<th id="price">' + cart.goods.gPrice + '</th>' +
            '<th width="100px">'+
            '<div class="input-group">'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-default" type="button" id="reduction">'+'-'+'</button>'+
            '<button style="display: none" id="cid" >'+ cart.cId +'</button>'+
            '</span>'+
            '<p type="text" class="form-control"  style="width:40px" id="cNum">'+ cart.cNum +
            '</p>'+
            '<span class="input-group-btn" id="addSpan">'+
            '<button class="btn btn-default" type="button" id="addCnum" >'+'+'+'</button>'+
            '</span>'+
            '</div>'+
            '</th>' +
            '<th id="all">' + '￥'+cart.cCount + '</th>'+
            '<th>' +  '<button type="button" class="btn btn-default" id="delete">'+ '删除'
            +'</button>' + '</th>';
        sum += cart.cCount;
    });
    $("#s_sum").html(sum);
}

//  渲染数据
function render(cartsList) {
    let sum = 0;
    $.each(cartsList, function (index, cart) {
        let tr;
        let num = index+1 ;
        // console.log(typeof index);
        // console.log(typeof num);
        console.log(cart);
        tr =
            '<th >' + num + '</th>' +
            '<th>' + cart.goods.gName + '</th>' +
            '<th>' + cart.goods.gPrice +'</th>' +
            '<th>' + cart.cNum + '</th>' +
            '<th>' + cart.cCount + '</th>' ;
        $("#div_carts_list").append('<tr>' + tr + '</tr>')
        sum += cart.cCount;
    });
    $("#s_sum").html(sum);
}

/*function render1(addressList){
    $.each(addressList, function (index, address) {
        let tr;
        tr = '<option value="address.aDetail">'+address.aName+'&nbsp;&nbsp;&nbsp;'+address.aTel+'&nbsp;&nbsp;&nbsp;'+address.aDetail +'</option>';
        $("#address").append(tr);
    });
}*/

//  渲染地址数据
function render1(addressList){
    $.each(addressList, function (index, address) {
        let tr;
        let num = index+1 ;
        tr =
            // '<th >' + '<span id="addaId" style="display: none">' + address.aId + '</span>' + '</th>' +
            '<th >' + num + '</th>' +
            '<th>' + address.aName + '</th>' +
            '<th>' + address.aTel + '</th>' +
            '<th id="price">' + address.aDetail + '</th>' +
            '<th>' +  '<button type="button" class="btn btn-default" id="submitOrder">'+ '选择地址并提交订单'
            +'</button>' +
            '<button style="display: none" id="addaId" >'+ address.aId +'</button>'+
            '</th>';
        $("#div_carts_list1").append('<tr id="father">' + tr + '</tr>')
    });
}

// 根据用户id清空购物车
function clearCart(){
    let user1 = getLocalStorage("loginUser");
    let url = "/cart/clearCart";
    // 这里的userId一定要和后台接口属性对应上
    let params = {
        userId : user1.uId };
    httpGet(url, params ,function (data) {
        //业务处理
        location.href = "cart.html";
    });
}

// 在头部哪里显示用户名称
function loadUserInfo() {
    let user = getLocalStorage("loginUser");
    $("#label_user_name").html(user.uName);
}

// 发起请求查询该用户购物车的数据
function LoadCartInfo() {
    //发起ajax请求
    let user1 = getLocalStorage("loginUser");
    let url = "/cart/selectCart";
    // 这里的userId一定要和后台接口属性对应上
    let params = {
        userId : user1.uId };
    httpGet(url, params ,function (data) {
        //业务处理
        renderingGoodsList(data);
    });
}
