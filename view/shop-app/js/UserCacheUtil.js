/**
 * 通用的数据存储
 * @param name
 * @param data
 */
//储存东西的通用方法
//存进去的是字符串
function saveSessionData(name,data) {
    let dataStr = JSON.stringify(data);
    //本地储存
    // localStorage.setItem(name, dataStr);
    //session储存
    sessionStorage.setItem(name, dataStr);
}

//取出会话信息的通用方法
//取出来之后转为json
function getSessionData(name) {
    //本地储存
    // let dataStr = localStorage.getItem(name);
    //session储存
    let dataStr = sessionStorage.getItem(name);
    let jsonData = JSON.parse(dataStr);
    // console.log("sessionStorage："+jsonData);
    return jsonData;
}

function removeSessionData(name) {
    sessionStorage.removeItem(name);
}


//储存东西的通用方法
//存进去的是字符串
function saveLocalData(name,data) {
    let dataStr = JSON.stringify(data);
    //本地储存
    localStorage.setItem(name, dataStr);
}

//取出会话信息的通用方法
//取出来之后转为json
function getLocalData(name) {
    //本地储存
    let dataStr = localStorage.getItem(name);
    let jsonData = JSON.parse(dataStr);
    // console.log("localStorage:"+jsonData);
    return jsonData;
}

function removeLocalData(name) {
    localStorage.removeItem(name);
}

/**
 * 下面是通用的方法
 */
//通用的取本地
function getLocalStorage(name) {

    let dataStr = sessionStorage.getItem(name);
    if (dataStr == null) {
        dataStr = localStorage.getItem(name);
    }

    //本地储存
    let jsonData = JSON.parse(dataStr);
    // console.log("localStorage:"+jsonData);
    return jsonData;
}


//通用的更新本地  在有用户登录的情况下
function updataLocalStorage(name,data) {

    if (sessionStorage.getItem(name) != null) {
        //session有 更新session中的数据
        saveSessionData(name, data);
    }else if (localStorage.getItem(name) != null) {
        saveLocalData(name, data);
    } else {
        //抛出异常
    }
}

//通用的更新本地  在有用户登录的情况下
function removeLocalStorage(name) {

    if (sessionStorage.getItem(name) != null) {
        //session有 更新session中的数据
        removeSessionData(name)
    }else if (localStorage.getItem(name) != null) {
        removeLocalData(name)
    } else {
        //抛出异常
    }
}