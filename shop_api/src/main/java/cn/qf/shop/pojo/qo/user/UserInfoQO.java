package cn.qf.shop.pojo.qo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户基本信息参数实体")
public class UserInfoQO {
    @ApiModelProperty(value = "用户id")
    private Integer uId;
    @ApiModelProperty(value = "用户名")
    private String uName;
    @ApiModelProperty(value = "用户密码")
    private String uPassword;
    @ApiModelProperty(value = "用户手机号")
    private String uPhone;
    @ApiModelProperty(value = "用户邮箱")
    private String uEmail;
    @ApiModelProperty(value = "用户性别")
    private String uSex;

    @Override
    public String toString() {
        return "UserInfoQO{" +
                "uId=" + uId +
                ", uName='" + uName + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uPhone='" + uPhone + '\'' +
                ", uEmail='" + uEmail + '\'' +
                ", uSex='" + uSex + '\'' +
                '}';
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuSex() {
        return uSex;
    }

    public void setuSex(String uSex) {
        this.uSex = uSex;
    }
}
