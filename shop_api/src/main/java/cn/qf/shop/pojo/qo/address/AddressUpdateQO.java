package cn.qf.shop.pojo.qo.address;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;

public class AddressUpdateQO {

    @ApiModelProperty("地址id")
    private Integer aId;
    @ApiModelProperty("用户姓名")
    private String aName;
    @ApiModelProperty("用户id")
    private Integer uId;
    @ApiModelProperty("用户手机号")
    private String aTel;
    @ApiModelProperty("是否默认地址")
    private Boolean aState;
    @ApiModelProperty("详细收货地址")
    private String aDetail;

    @Override
    public String toString() {
        return "AddressUpdateQO{" +
                "aId='" + aId + '\'' +
                ", aName='" + aName + '\'' +
                ", uId=" + uId +
                ", aTel='" + aTel + '\'' +
                ", aState=" + aState +
                ", aDetail='" + aDetail + '\'' +
                '}';
    }

    public Integer getaId() { return aId; }

    public void setaId(Integer aId) {
        this.aId = aId;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getaTel() {
        return aTel;
    }

    public void setaTel(String aTel) {
        this.aTel = aTel;
    }

    public Boolean getaState() {
        return aState;
    }

    public void setaState(Boolean aState) {
        this.aState = aState;
    }

    public String getaDetail() {
        return aDetail;
    }

    public void setaDetail(String aDetail) {
        this.aDetail = aDetail;
    }
}
