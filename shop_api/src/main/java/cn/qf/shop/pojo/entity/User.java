package cn.qf.shop.pojo.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class User implements Serializable {
    //用户主键id
    private Integer uId;
    //用户名
    private String uName;
    //用户密码
    private String uPassword;
    //用户手机
    private String uPhone;
    //用户邮箱
    private String uEmail;
    //用户性别
    private String uSex;
    //用户注册激活状态
    private Integer uStatus = 0; //默认未激活
    //用户激活码
    private String uCode;
    //用户金额
    private BigDecimal uBalance= BigDecimal.valueOf(0);
    //用户等级
    private Integer uLevel = 1;

    @Override
    public String toString() {
        return "User{" +
                "uId=" + uId +
                ", uName='" + uName + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uPhone='" + uPhone + '\'' +
                ", uEmail='" + uEmail + '\'' +
                ", uSex='" + uSex + '\'' +
                ", uStatus=" + uStatus +
                ", uCode='" + uCode + '\'' +
                ", uBalance=" + uBalance +
                ", uLevel=" + uLevel +
                '}';
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuSex() {
        return uSex;
    }

    public void setuSex(String uSex) {
        this.uSex = uSex;
    }

    public Integer getuStatus() {
        return uStatus;
    }

    public void setuStatus(Integer uStatus) {
        this.uStatus = uStatus;
    }

    public String getuCode() {
        return uCode;
    }

    public void setuCode(String uCode) {
        this.uCode = uCode;
    }

    public BigDecimal getuBalance() {
        return uBalance;
    }

    public void setuBalance(BigDecimal uBalance) {
        this.uBalance = uBalance;
    }

    public Integer getuLevel() {
        return uLevel;
    }

    public void setuLevel(Integer uLevel) {
        this.uLevel = uLevel;
    }
}
