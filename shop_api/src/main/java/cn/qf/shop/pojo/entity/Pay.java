package cn.qf.shop.pojo.entity;

/**
 * 支付宝参数实体类
 */
public class Pay {
    private String outTradeNo;//商家订单号
    private Double totalAmount;//付款金额
    private String subject;//订单名称
    private String body;//商品描述
    private Integer timeoutExpress = 1;//订单的超时时间,单位是分钟

    public Pay() {
    }

    public Pay(String outTradeNo, Double totalAmount, String subject, String body) {
        this.outTradeNo = outTradeNo;
        this.totalAmount = totalAmount;
        this.subject = subject;
        this.body = body;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getTimeoutExpress() {
        return timeoutExpress;
    }

    public void setTimeoutExpress(Integer timeoutExpress) {
        this.timeoutExpress = timeoutExpress;
    }

    @Override
    public String toString() {
        return "Pay{" +
                "outTradeNo='" + outTradeNo + '\'' +
                ", totalAmount=" + totalAmount +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", timeoutExpress=" + timeoutExpress +
                '}';
    }
}
