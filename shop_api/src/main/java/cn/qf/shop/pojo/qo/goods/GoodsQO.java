package cn.qf.shop.pojo.qo.goods;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel("商品模块参数实体")
public class GoodsQO {


    @ApiModelProperty(value = "用户ID")
    private int uId;

    @ApiModelProperty(value = "商品ID")
    private int gId;

    @ApiModelProperty(value = "商品的名称")
    private String  gName;

    @ApiModelProperty(value = "商品的上市时间")
    private Date gTime;

    @ApiModelProperty(value = "商品图片的路径")
    private String gImage;


    @ApiModelProperty(value = "商品的价格")
    private BigDecimal gPrice;

    @ApiModelProperty(value = "商品的热门指数")
    private int gState ;

    @ApiModelProperty(value = "商品的描述")
    private String gInfo;

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public int getgId() {
        return gId;
    }

    public void setgId(int gId) {
        this.gId = gId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public Date getgTime() {
        return gTime;
    }

    public void setgTime(Date gTime) {
        this.gTime = gTime;
    }

    public String getgImage() {
        return gImage;
    }

    public void setgImage(String gImage) {
        this.gImage = gImage;
    }

    public BigDecimal getgPrice() {
        return gPrice;
    }

    public void setgPrice(BigDecimal gPrice) {
        this.gPrice = gPrice;
    }

    public int getgState() {
        return gState;
    }

    public void setgState(int gState) {
        this.gState = gState;
    }

    public String getgInfo() {
        return gInfo;
    }

    public void setgInfo(String gInfo) {
        this.gInfo = gInfo;
    }

    @Override
    public String toString() {
        return "GoodsQO{" +
                "uId=" + uId +
                ", gId=" + gId +
                ", gName='" + gName + '\'' +
                ", gTime=" + gTime +
                ", gImage='" + gImage + '\'' +
                ", gPrice=" + gPrice +
                ", gState=" + gState +
                ", gInfo='" + gInfo + '\'' +
                '}';
    }
}
