package cn.qf.shop.pojo.entity;

/**
 * aid 地址主键
 * aname 收货人
 *  收货地址
 * User 用户实体  电话
 *
 *
 */
public class Address {
    /**
    * 地址主键 id
    */
    private Integer aId;

    /**
     * 收货人名称
     */
    private String aName;

    /**
     * 用户表的用户id
     */
    private Integer uId;

    /**
     * 用户的收货号码
     */
    private String aTel;


    /**
    * 逻辑删除 0为不删除1为删除
    */
    private Boolean aDeleted;

    /**
     * 是否默认地址 0为默认地址 1为非默认地址
     */
    private Boolean aState;

    /**
     * 详细收货地址
     */
    private String aDetail;

    @Override
    public String toString() {
        return "Address{" +
                "aId=" + aId +
                ", aName='" + aName + '\'' +
                ", uId=" + uId +
                ", aTel='" + aTel + '\'' +
                ", aDeleted=" + aDeleted +
                ", aState=" + aState +
                ", aDetail='" + aDetail + '\'' +
                '}';
    }

    public Integer getaId() {
        return aId;
    }

    public void setaId(Integer aId) {
        this.aId = aId;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getaTel() {
        return aTel;
    }

    public void setaTel(String aTel) {
        this.aTel = aTel;
    }

    public Boolean getaDeleted() {
        return aDeleted;
    }

    public void setaDeleted(Boolean aDeleted) {
        this.aDeleted = aDeleted;
    }

    public Boolean getaState() {
        return aState;
    }

    public void setaState(Boolean aState) {
        this.aState = aState;
    }

    public String getaDetail() {
        return aDetail;
    }

    public void setaDetail(String aDetail) {
        this.aDetail = aDetail;
    }
}