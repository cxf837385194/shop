package cn.qf.shop.pojo.qo.cart;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("根据用户id和商品id添加商品到购物车")
public class CartAddQO {

    @ApiModelProperty("用户id")
    private Integer uId;

    @ApiModelProperty("商品数量")
    private Integer gId;

    @Override
    public String toString() {
        return "CartAddQO{" +
                "uId=" + uId +
                ", gId=" + gId +
                '}';
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getgId() {
        return gId;
    }

    public void setgId(Integer gId) {
        this.gId = gId;
    }
}
