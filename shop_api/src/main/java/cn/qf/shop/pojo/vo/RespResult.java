package cn.qf.shop.pojo.vo;

/**
 * 返回数据标准实体类
 *
 * @param <T> 业务处理返回数据类型（泛型）
 */
public class RespResult<T> {
    //业务处理状态0：成功，其它：失败
    private int code = Code.SUCCESS.getValue();
    //提示信息（错误或者异常提示用户信息）
    private String message;
    //业务返回数据
    private T data;

    /**
     * 业务处理状态枚举
     */
    public enum Code {
        SUCCESS(0), FAILED(-1);
        private int value;

        Code(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
