package cn.qf.shop.pojo.qo.cart;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("修改商品的数量及小计")
public class CartUpdateQO {

    @ApiModelProperty("购物车Id")
    private Integer cId ;
    @ApiModelProperty("用户id")
    private Integer cNum ;
    @ApiModelProperty("商品单价")
    private Integer gPrice ;

    @Override
    public String toString() {
        return "CartUpdateQO{" +
                "cId=" + cId +
                ", cNum=" + cNum +
                ", gPrice=" + gPrice +
                '}';
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public Integer getcNum() {
        return cNum;
    }

    public void setcNum(Integer cNum) {
        this.cNum = cNum;
    }

    public Integer getgPrice() {
        return gPrice;
    }

    public void setgPrice(Integer gPrice) {
        this.gPrice = gPrice;
    }
}
