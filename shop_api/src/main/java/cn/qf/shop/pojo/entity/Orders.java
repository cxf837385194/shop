package cn.qf.shop.pojo.entity;



import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单模块实体类
 * 实现序列化
 */
public class Orders implements Serializable {


    private static  final long serialVersionUID = 1L;

    //用户模块主键
    private Integer uId ;

    //地址模块主键
    private Integer aId ;

    //关联的地址模块(以获取详细收货地址)
    private Address address ;

    //订单编号
    private String oId ;

    //订单创建时间
    private Date oCreateTime ;


    //订单修改时间
    private Date oUpdateTime ;

    //订单的总金额
    private BigDecimal oCount ;

    //订单状态
    private Integer oState ;

    //订单评价
    private String oAssess ;


    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getaId() {
        return aId;
    }

    public void setaId(Integer aId) {
        this.aId = aId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getoId() {
        return oId;
    }

    public void setoId(String oId) {
        this.oId = oId;
    }

    public Date getoCreateTime() {
        return oCreateTime;
    }

    public void setoCreateTime(Date oCreateTime) {
        this.oCreateTime = oCreateTime;
    }

    public Date getoUpdateTime() {
        return oUpdateTime;
    }

    public void setoUpdateTime(Date oUpdateTime) {
        this.oUpdateTime = oUpdateTime;
    }

    public BigDecimal getoCount() {
        return oCount;
    }

    public void setoCount(BigDecimal oCount) {
        this.oCount = oCount;
    }

    public Integer getoState() {
        return oState;
    }

    public void setoState(Integer oState) {
        this.oState = oState;
    }

    public String getoAssess() {
        return oAssess;
    }

    public void setoAssess(String oAssess) {
        this.oAssess = oAssess;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "uId=" + uId +
                ", aId=" + aId +
                ", address=" + address +
                ", oId='" + oId + '\'' +
                ", oCreateTime=" + oCreateTime +
                ", oUpdateTime=" + oUpdateTime +
                ", oCount=" + oCount +
                ", oState=" + oState +
                ", oAssess='" + oAssess + '\'' +
                '}';
    }
}
