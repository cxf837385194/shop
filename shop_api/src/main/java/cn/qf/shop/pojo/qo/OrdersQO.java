package cn.qf.shop.pojo.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel("订单模块参数实体")
public class OrdersQO {

    @ApiModelProperty(value = "用户ID")
    private Integer uId ;

    @ApiModelProperty(value = "地址ID")
    private Integer aId ;

    @ApiModelProperty(value = "订单编号")
    private String oId ;

    @ApiModelProperty(value = "订单状态")
    private Integer oState ;

    @ApiModelProperty(value = "商品ID")
    private Integer gId ;

    @ApiModelProperty(value = "订单评价")
    private String oAssess ;

    @ApiModelProperty(value = "订单总金额")
    private BigDecimal oCount ;

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getaId() {
        return aId;
    }

    public void setaId(Integer aId) {
        this.aId = aId;
    }

    public String getoId() {
        return oId;
    }

    public void setoId(String oId) {
        this.oId = oId;
    }

    public Integer getoState() {
        return oState;
    }

    public void setoState(Integer oState) {
        this.oState = oState;
    }

    public Integer getgId() {
        return gId;
    }

    public void setgId(Integer gId) {
        this.gId = gId;
    }

    public String getoAssess() {
        return oAssess;
    }

    public void setoAssess(String oAssess) {
        this.oAssess = oAssess;
    }

    public BigDecimal getoCount() {
        return oCount;
    }

    public void setoCount(BigDecimal oCount) {
        this.oCount = oCount;
    }

    @Override
    public String toString() {
        return "OrdersQO{" +
                "uId=" + uId +
                ", aId=" + aId +
                ", oId='" + oId + '\'' +
                ", oState=" + oState +
                ", gId=" + gId +
                ", oAssess='" + oAssess + '\'' +
                ", oCount=" + oCount +
                '}';
    }
}
