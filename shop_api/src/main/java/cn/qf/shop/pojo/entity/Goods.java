package cn.qf.shop.pojo.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品实体类
 */
public class Goods {

    private int gId;    // 商品的唯一主键

    private int tId;    // 类别的主键id

    private String  gName;  // 商品的名称

    private Date gTime;  // 商品的上市时间

    private String gImage;  //商品图片的路径

    private BigDecimal gPrice;  //商品的价格

    private int gState ; //商品的热门指数

    private String gInfo; //商品的描述

    public Goods() {
    }

    @Override
    public String toString() {
        return "Goods{" +
                "gId=" + gId +
                ", tId=" + tId +
                ", gName='" + gName + '\'' +
                ", gTime=" + gTime +
                ", gImage='" + gImage + '\'' +
                ", gPrice=" + gPrice +
                ", gState=" + gState +
                ", gInfo='" + gInfo + '\'' +
                '}';
    }

    public int getgId() {
        return gId;
    }

    public void setgId(int gId) {
        this.gId = gId;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public Date getgTime() {
        return gTime;
    }

    public void setgTime(Date gTime) {
        this.gTime = gTime;
    }

    public String getgImage() {
        return gImage;
    }

    public void setgImage(String gImage) {
        this.gImage = gImage;
    }

    public BigDecimal getgPrice() {
        return gPrice;
    }

    public void setgPrice(BigDecimal gPrice) {
        this.gPrice = gPrice;
    }

    public int getgState() {
        return gState;
    }

    public void setgState(int gState) {
        this.gState = gState;
    }

    public String getgInfo() {
        return gInfo;
    }

    public void setgInfo(String gInfo) {
        this.gInfo = gInfo;
    }
}
