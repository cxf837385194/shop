package cn.qf.shop.pojo.qo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户登录参数实体")
public class UserLoginQO {

    @ApiModelProperty(value = "用户名")
    private String uName;
    @ApiModelProperty(value = "用户密码")
    private String uPassword;
    @ApiModelProperty(value = "用户手机号")
    private String uPhone;
    @ApiModelProperty(value = "短信验证码")
    private String noteCode;
    @ApiModelProperty(value = "图片验证码")
    private String vcode;

    @Override
    public String toString() {
        return "UserLoginQO{" +
                "uName='" + uName + '\'' +
                ", uPassword='" + uPassword + '\'' +
                ", uPhone='" + uPhone + '\'' +
                ", noteCode='" + noteCode + '\'' +
                ", vcode='" + vcode + '\'' +
                '}';
    }

    public String getNoteCode() {
        return noteCode;
    }

    public void setNoteCode(String noteCode) {
        this.noteCode = noteCode;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }
}
