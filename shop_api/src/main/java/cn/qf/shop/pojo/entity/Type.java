package cn.qf.shop.pojo.entity;

import java.io.Serializable;

/**
 * 商品类别实体表
 */
public class Type  {
    private int tId;  //类别的主键id

    private String tName; //类别的名称

    private String tInfo;  //类别的描述

    @Override
    public String toString() {
        return "Type{" +
                "tId=" + tId +
                ", tName='" + tName + '\'' +
                ", tInfo='" + tInfo + '\'' +
                '}';
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public String gettInfo() {
        return tInfo;
    }

    public void settInfo(String tInfo) {
        this.tInfo = tInfo;
    }
}
