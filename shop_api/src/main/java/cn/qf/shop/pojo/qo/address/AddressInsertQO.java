package cn.qf.shop.pojo.qo.address;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("真-用户添加地址实体")
public class AddressInsertQO {
    @ApiModelProperty("用户姓名")
    private String aName;
    @ApiModelProperty("用户id")
    private Integer uId;
    @ApiModelProperty("用户手机号")
    private String aTel;
    @ApiModelProperty("详细收货地址")
    private String aDetail;

    @Override
    public String toString() {
        return "AddressInsertQO{" +
                "aName='" + aName + '\'' +
                ", uId=" + uId +
                ", aTel='" + aTel + '\'' +
                ", aDetail='" + aDetail + '\'' +
                '}';
    }

    public String getaDetail() {
        return aDetail;
    }

    public void setaDetail(String aDetail) {
        this.aDetail = aDetail;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getaTel() {
        return aTel;
    }

    public void setaTel(String aTel) {
        this.aTel = aTel;
    }

}