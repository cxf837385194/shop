package cn.qf.shop.pojo.entity;

import java.math.BigDecimal;

/**
 * 购物车实体类
 */
public class Cart {

    // 购物车主键
    private int cId;

    // 用户id
    private int uId;

    // 商品id
    private int gId;

    // 购物车小计
    private BigDecimal cCount;

    //购物车商品数量
    private int cNum;

    // 用户
    private User user;

    // 商品
    private Goods goods;

    @Override
    public String toString() {
        return "Cart{" +
                "cId=" + cId +
                ", cCount=" + cCount +
                ", cNum=" + cNum +
                ", user=" + user +
                ", goods=" + goods +
                '}';
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public int getgId() {
        return gId;
    }

    public void setgId(int gId) {
        this.gId = gId;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public BigDecimal getcCount() {
        return cCount;
    }

    public void setcCount(BigDecimal cCount) {
        this.cCount = cCount;
    }

    public int getcNum() {
        return cNum;
    }

    public void setcNum(int cNum) {
        this.cNum = cNum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }
}
