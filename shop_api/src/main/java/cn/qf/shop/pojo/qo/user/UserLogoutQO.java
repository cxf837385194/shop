package cn.qf.shop.pojo.qo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("用户注销实体")
public class UserLogoutQO {

    @ApiModelProperty(value = "用户id")
    private Integer uId;
    @ApiModelProperty(value = "用户名")
    private String uName;
    @ApiModelProperty(value = "用户手机号")
    private String uPhone;
    @ApiModelProperty(value = "短信验证码")
    private String noteCode;

    @Override
    public String toString() {
        return "UserLogoutQO{" +
                "uId=" + uId +
                ", uName='" + uName + '\'' +
                ", uPhone='" + uPhone + '\'' +
                ", noteCode='" + noteCode + '\'' +
                '}';
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getNoteCode() {
        return noteCode;
    }

    public void setNoteCode(String noteCode) {
        this.noteCode = noteCode;
    }
}
