package cn.qf.shop.service;

import cn.qf.shop.pojo.entity.User;
import cn.qf.shop.pojo.qo.user.UserInfoQO;
import cn.qf.shop.pojo.qo.user.UserLoginQO;
import cn.qf.shop.pojo.qo.user.UserLogoutQO;
import cn.qf.shop.pojo.qo.user.UserRegisterQO;

public interface UserService {
    /**
     * 注册用户
     * @param qo
     * @return 返回用户 或者 null 我也不知道用来干嘛
     */
    User registerUser(UserRegisterQO qo);

    User loginUser(UserLoginQO qo);

    User activateUser(String code);

    User verifyActive(String uName);

    User checkTelByUserName(UserLoginQO qo);

    User smsLoginUser(UserLoginQO qo);

    User userInfoUpData(UserInfoQO qo);

    String logoutUser(UserLogoutQO qo);
}
