package cn.qf.shop.service.impl;

import cn.qf.shop.mapper.OrdersMapper;
import cn.qf.shop.pojo.entity.Cart;
import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.entity.Orders;
import cn.qf.shop.pojo.qo.OrdersQO;
import cn.qf.shop.pojo.qo.goods.GoodsQO;
import cn.qf.shop.service.GoodsService;
import cn.qf.shop.service.OrdersService;
import cn.qf.shop.utils.RandomUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * 订单模块业务层实现类
 * 存储于spring容器中
 */
@Service
public class OrdersServiceImpl implements OrdersService {

    /**
     * 初始化订单模块数据访问层对象
     */
    @Resource
    private OrdersMapper ordersMapper;



    /**
     * 初始化商品模块数据访问层对象
     */
    @Resource
    private GoodsService goodsService;



    @Override
    public List<Orders> selectAllOrders(OrdersQO ordersQO) {

        List<Orders> ordersList = ordersMapper.selectAllOrders(ordersQO.getuId());

        return ordersList;
    }

    @Override
    public Orders selectByOid(OrdersQO ordersQO) {

        Orders orders = ordersMapper.selectByOid(ordersQO.getuId(),ordersQO.getoId());

        return orders;
    }

    @Override
    public void updateStateByOid(OrdersQO ordersQO) {

    }

    @Override
    public List<Orders> selectByOstate(OrdersQO ordersQO) {

        List<Orders> ordersList = ordersMapper.selectByOstate(ordersQO.getuId(),ordersQO.getoState());

        return ordersList ;
    }

    @Override
    public void updateAssessByOid(OrdersQO ordersQO) {

        ordersMapper.updateAssessByOid(ordersQO.getoId(),ordersQO.getoAssess());
    }

    @Override
    public void deleteOrderByOid(OrdersQO ordersQO) {
        ordersMapper.deleteOrderByOid(ordersQO.getoId());
    }



    @Override
    public void addOrderByOid(OrdersQO ordersQO) {

        //控制台打印从前端获取的参数
        //1用户ID
        System.out.println("用户ID："+ordersQO.getgId());
        System.out.println("地址ID："+ordersQO.getaId());
        System.out.println("总金额："+ordersQO.getoCount());

        //调用工具类随机生成订单编号
        String oId = RandomUtils.createOrderId();

        ordersMapper.addOrderByOid(ordersQO.getuId() , ordersQO.getaId() ,oId ,ordersQO.getoCount());
    }

    @Override
    public void selectGoodsByGid(GoodsQO goodsQO) {
        //1.获取前端发送请求的参数  商品ID gId  用户ID uId
        Integer gId = goodsQO.getgId();
        Integer uId = goodsQO.getuId();

        //调用dao层进行非空判断
        Cart cart = ordersMapper.selectGoodsByGid(gId);
        System.out.println("cart=" + cart);




        //根据商品ID查询到商品信息
        Goods goods = goodsService.selectByGId(goodsQO);

        //根据查询到的商品信息获取商品的单价
        BigDecimal gPrice = goods.getgPrice();

        //非空判断
        if (cart != null) {
            //查询到了数据
            /*
             * 修改数据库数据
             *   1.商品的小计(当前的小计+商品的单价)  //根据商品ID查询获取商品的单价
             *   2.商品的数量
             * */

            //1.获取商品的单价

            //根据查询到的购物车信息获取该商品的小计
            BigDecimal cCount = cart.getcCount();

            //根据查询到的购物车信息获取该商品的数量
            int cNum = cart.getcNum();

            //向数据库更新的小计
            BigDecimal count = cCount.add(gPrice);

            //2.获取商品的数量
            //点击一次按钮，就是一次请求，后端接口上做商品数量加1的操作
            //向数据库更新的商品数量
            int num = cNum + 1 ;

            //3.调用dao层做数据修改操作
            ordersMapper.upCartNumAndCartCountByOid(gId,count,num);
        }else {
            //购物车表中不存在该商品信息
            //向数据库中写入数据
            //1.用户ID
            //2.商品ID
            //3.商品价格
            //4.商品数量 初始值为1
            ordersMapper.addGoodsToCart(uId,gId,gPrice);
        }

    }

    @Override
    public void updatePayStatus(String oId) {
        System.out.println("业务层获取的订单编号:");
        System.out.println(oId);
        ordersMapper.updatePayStatus(oId);
    }

    @Override
    public void chageOstateTwo(OrdersQO ordersQO) {
        ordersMapper.chageOstateTwo(ordersQO.getoId());
    }

    @Override
    public void chageOstateFour(OrdersQO ordersQO) {
        ordersMapper.chageOstateFour(ordersQO.getoId());
    }
}
