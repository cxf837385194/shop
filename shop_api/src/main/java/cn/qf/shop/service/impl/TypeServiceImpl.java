package cn.qf.shop.service.impl;

import cn.qf.shop.mapper.TypeMapper;
import cn.qf.shop.mapper.UserMapper;
import cn.qf.shop.pojo.entity.Type;
import cn.qf.shop.service.TypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    private Logger logger = LoggerFactory.getLogger(TypeServiceImpl.class);

    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<Type> findAll() {

        return typeMapper.selectTypeAll();
    }

    @Override
    public Type selectByTid(Type type) {
        Type typeId = typeMapper.selectTypeById(type.gettId());
        return typeId;
    }
}
