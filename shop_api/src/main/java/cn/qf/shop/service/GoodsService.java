package cn.qf.shop.service;

import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.qo.goods.GoodsQO;

import java.util.List;

public interface GoodsService {

    /**
     * 获取所有商品列表
     *
     * @return 如果无： list size：0
     */
    List<Goods> listAll();


    /**
     *
     * 根据商品id查询商品数据
     * @param qo 参数实体（传递gId）
     * @return  从数据库中查询的商品数据
     */
    Goods selectByGId(GoodsQO qo);


    /**
     * 根据商品类别tid查询商品数据
     * @param goods 商品实体（传递tid）
     * @return
     */
    List<Goods> selectByTid(Goods goods);
}
