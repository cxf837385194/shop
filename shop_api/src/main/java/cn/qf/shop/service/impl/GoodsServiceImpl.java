package cn.qf.shop.service.impl;

import cn.qf.shop.mapper.GoodsMapper;
import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.qo.goods.GoodsQO;
import cn.qf.shop.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {


    private Logger logger = LoggerFactory.getLogger(GoodsServiceImpl.class);

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<Goods> listAll() {

        return goodsMapper.selectAll();
    }

    @Override
    public Goods selectByGId(GoodsQO qo) {
        Goods goods = goodsMapper.selectGoodsById(qo.getgId());
        return goods;
    }

    @Override
    public List<Goods> selectByTid(Goods goods) {
        List<Goods> goodsList = goodsMapper.selectGoodsByTid(goods.gettId());

        return goodsList;
    }
}
