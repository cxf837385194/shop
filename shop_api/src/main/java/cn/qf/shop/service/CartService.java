package cn.qf.shop.service;

import cn.qf.shop.pojo.entity.Cart;

import java.math.BigDecimal;
import java.util.List;

public interface CartService {

    /**
     * 通过用户id查询购物车数据
     * @param userId
     * @return
     */
    List<Cart> selectCartByUserId(Long userId);

    /**
     * 通过用户id和商品id添加商品到购物车
     * @param userId
     * @param pid
     * @return
     */
    int insertCart(Integer uId, Integer gid);

    /**
     * 通过购物车id删除购物车的商品
     * @param cId
     * @return
     */
    int deleteCartByCid(Integer cId);

    /**
     * 根据用户id清空该用户购物车
     * @param userId
     * @return
     */
    int clearCart(Integer userId);

    /**
     * 修改商品的数量
     * @param cId
     * @param cNum
     * @param gPrice1
     * @return
     */
    int updateCartByCid(Integer cId, Integer cNum, BigDecimal gPrice1);

    /**
     * 根据用户id和商品id查询购物车数据
     * @param uId
     * @param gId
     * @return
     */
    Cart selectCartByCid(Integer uId, Integer gId);
}
