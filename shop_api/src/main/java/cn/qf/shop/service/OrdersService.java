package cn.qf.shop.service;


import cn.qf.shop.pojo.entity.Orders;
import cn.qf.shop.pojo.qo.OrdersQO;
import cn.qf.shop.pojo.qo.goods.GoodsQO;

import java.util.List;

/**
 * 订单模块业务层接口
 */
public interface OrdersService {

    /**
     * 查询所有订单数据，
     * 用于订单页面数据展示
     * @return 从数据库中查询到的所有订单信息
     */
    List<Orders> selectAllOrders(OrdersQO ordersQO);

    /**
     * 根据订单编号查询订单数据
     * @param ordersQO 参数实体（传递oId）
     * @return 从数据库中查询到的订单数据
     */
    Orders selectByOid(OrdersQO ordersQO);

    /**
     * 修改订单的状态
     * @param ordersQO 参数实体（传递oId）
     */
    void updateStateByOid(OrdersQO ordersQO);

    /**
     * 根据订单状态查询
     * @param ordersQO 参数实体（传递oState）
     * @return 从数据库中获取的订单信息
     */
    List<Orders> selectByOstate(OrdersQO ordersQO);

    /**
     * 定点订单编号修改订单评价
     * @param ordersQO
     */
    void updateAssessByOid(OrdersQO ordersQO);

    /**
     * 定点订单编号删除订单
     * @param ordersQO
     */
    void deleteOrderByOid(OrdersQO ordersQO);


    /**
     * 定点用户ID新增订单
     * @param ordersQO
     */
    void addOrderByOid(OrdersQO ordersQO);

    /**
     * 根据商品ID在购物车表中
     * 查询商品信息
     * @param goodsQO  参数： 商品ID （oId）
     */
    void selectGoodsByGid(GoodsQO goodsQO);


    /**
     * 根据订单编号查询订单
     * 修改 订单状态
     * 从 0（待支付状态） 改为 1（已支付，待发货）
     * @param oId
     */
    void updatePayStatus(String oId);

    /**
     * 发货
     * @param ordersQO
     */
    void chageOstateTwo(OrdersQO ordersQO);

    /**
     * 收货
     * @param ordersQO
     */
    void chageOstateFour(OrdersQO ordersQO);
}
