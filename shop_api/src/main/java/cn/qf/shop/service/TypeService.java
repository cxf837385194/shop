package cn.qf.shop.service;

import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.entity.Type;
import cn.qf.shop.pojo.qo.goods.GoodsQO;

import java.util.List;

public interface TypeService {

    /**
     * 获得所有类别列表
     *
     * @return
     */
    List<Type> findAll();


    /**
     * 根据类别Id 查询商品类别数据
     * @param type 类别实体 （传递Tid）
     * @return 从数据库中查询到的类别数据
     */
    Type selectByTid (Type type);
}
