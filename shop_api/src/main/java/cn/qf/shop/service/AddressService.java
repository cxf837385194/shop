package cn.qf.shop.service;

import cn.qf.shop.pojo.entity.Address;
import cn.qf.shop.pojo.qo.address.AddressAddQO;
import cn.qf.shop.pojo.qo.address.AddressInsertQO;
import cn.qf.shop.pojo.qo.address.AddressUpdateQO;

import java.util.List;

public interface AddressService {
    /**
     * 根据用户id查询所有地址
     * @param
     * @return
     */
//    List<Address> selectAddressByUid(AddressAddQO addressAddQO);
    List<Address> selectAddressByQO(AddressAddQO addressAddQO);

    /**
     * 根据用户id查询所有地址
     *
     * @param uId
     * @return
     */
    List<Address> selectAddressByUid(Integer uId);

    /**
     * 根据用户id添加用户id
     *
     * @param uId
     * @return
     */
    int insertAddress(AddressAddQO addressAddQO);

    /**
     * 根据地址Aid修改地址信息
     *
     * @param addressUpdateQO 参数实体(传递参数Aid)
     * @return
     */
    int updateByAid(AddressUpdateQO addressUpdateQO);

    /**
     * 根据地址Aid查询地址信息
     *
     * @param address
     * @return
     */
    Address selectAddressByAid(Address address);


    int insertAddressPost(AddressInsertQO address);

    /**
     *根据aid假删除
     * @param aId
     * @return
     */
    int deleteArByAid(Integer aId);

}