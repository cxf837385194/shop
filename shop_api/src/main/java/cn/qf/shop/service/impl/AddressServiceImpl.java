package cn.qf.shop.service.impl;

import cn.qf.shop.mapper.AddressMapper;
import cn.qf.shop.pojo.entity.Address;
import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.qo.address.AddressAddQO;
import cn.qf.shop.pojo.qo.address.AddressInsertQO;
import cn.qf.shop.pojo.qo.address.AddressUpdateQO;
import cn.qf.shop.pojo.qo.goods.GoodsQO;
import cn.qf.shop.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressMapper addressMapper;

    //
//    @Override
//    public List<Address> selectAddressByUid(AddressAddQO addressAddQO) {
//        List<Address> address = addressMapper.selectAddressByUid(addressAddQO.getuId());
//        if(address.size()==0){
//            throw new RuntimeException("该用户没有地址");
//        }
//        return address;
//    }


    @Override
    public List<Address> selectAddressByQO(AddressAddQO addressAddQO) {
        List<Address> address = addressMapper.selectAddressByUid(addressAddQO.getuId());
        if(address.size()==0){
            throw new RuntimeException("该用户没有地址");
        }
        return address;
    }

    @Override
    public List<Address> selectAddressByUid(Integer uId) {
        List<Address> address = addressMapper.selectAddressByUid(uId);
        if (address.size() == 0) {
            throw new RuntimeException("该用户没有地址");
        }
        return address;
    }

    @Override
    public int insertAddress(AddressAddQO addressAddQO) {
        return 0;
    }

    @Override
    public int updateByAid(AddressUpdateQO addressUpdateQO) {
//报错的地方---
//        addressMapper.updateByAid(addressUpdateQO);
        //  ordersMapper.updateAssessByOid(ordersQO.getoId(),ordersQO.getoAssess());
        int result = addressMapper.upDataByUpdateQO(addressUpdateQO);
        return result;
    }

    @Override
    public Address selectAddressByAid(Address address) {
        Address newAddress = addressMapper.selectByAId(address.getaId());
        return newAddress;
    }

    @Override
    public int insertAddressPost(AddressInsertQO addressQo) {
        //转为address
        Address address = new Address();
        address.setuId(addressQo.getuId());
        address.setaDetail(addressQo.getaDetail());
        address.setaName(addressQo.getaName());
        address.setaTel(addressQo.getaTel());
        System.out.println("address = " + address);

        int result = addressMapper.insertAddressPost(address);


        return result;

    }


    @Override
    public int deleteArByAid(Integer aId) {
        int i=addressMapper.deleteArByAid(aId);
        if(i==0){
            throw new RuntimeException("aid不存在");
        }
        return i;
    }
//    @Override
//    public int insertAddress(AddressAddQO addressAddQO) {
//        int i = addressMapper.insertAddress(addressAddQO);
//        if(i==0){
//            throw new RuntimeException("添加失败");
//        }
//        return i;
//    }
//
//    @Override
//    public int updateByAid(AddressUpdateQO addressUpdateQO) {
//        int i=addressMapper.updateByAid(addressUpdateQO);
//        if(i==0){
//            throw new RuntimeException("修改失败");
//        }
//        return i;
//    }
}