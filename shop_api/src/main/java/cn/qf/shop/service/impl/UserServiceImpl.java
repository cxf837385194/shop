package cn.qf.shop.service.impl;

import cn.qf.shop.mapper.UserMapper;
import cn.qf.shop.pojo.entity.User;
import cn.qf.shop.pojo.qo.user.UserInfoQO;
import cn.qf.shop.pojo.qo.user.UserLoginQO;
import cn.qf.shop.pojo.qo.user.UserLogoutQO;
import cn.qf.shop.pojo.qo.user.UserRegisterQO;
import cn.qf.shop.service.UserService;
import cn.qf.shop.utils.EmailUtils;
import cn.qf.shop.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User registerUser(UserRegisterQO qo) {
        //初始化用户
        User user = new User();
        user.setuName(qo.getuName());
        user.setuPassword(qo.getuPassword());
        user.setuEmail(qo.getuEmail());
        user.setuSex(qo.getuSex());
        user.setuPhone(qo.getuPhone());
        //设置用户码
        user.setuCode(RandomUtils.createActive());
        System.out.println("运行到mapper上面！！");
        System.out.println("user = " + user);
        userMapper.insertUser(user);
        System.out.println("运行到mapper下面！！");
        System.out.println("user = " + user);
        //2.发生一封邮件
        EmailUtils.sendEmail(user);
        return user;
    }

    /**
     * 用户登录验证
     * @param qo
     * @return
     */
    @Override
    public User loginUser(UserLoginQO qo) {
        //根据用户名查询用户
        User user = userMapper.selectUserByName(qo.getuName());
        System.out.println("user = " + user);
        //对比密码是否正确
        if (!qo.getuPassword().equals(user.getuPassword())) {
            //密码错误
            return null;
        }
        if (user.getuStatus() == 0) {
            //用户未激活
            return null;
        }
        //返回用户
        System.out.println("return user = " + user);
        return user;
    }

    @Override
    public User activateUser(String code) {
        //根据code查询用户
        User user = userMapper.selectUserByCode(code);
        //查到后设置激活 激活状态1
        user.setuStatus(1);
        System.out.println("user = " + user);
        //更新数据库状态
        int result = userMapper.updateUserByUId(user);
        //返回user
        if (result != 0) {
            return user;
        }

        return null;
    }

    @Override
    public User verifyActive(String uName) {

        User user = userMapper.selectUserByName(uName);
        //已激活就返回用户
        if (user.getuStatus() == 1) {
            return user;
        }
        //未激活就返回空
        return null;
    }

    @Override
    public User checkTelByUserName(UserLoginQO qo) {

        //验证手机号和用户名是否匹配
        User user = userMapper.selectUserByName(qo.getuName());
        if (!user.getuPhone().equals(qo.getuPhone())) {
            return null;
        }
        //匹配返回用户
        return user;
    }

    @Override
    public User smsLoginUser(UserLoginQO qo) {
        User user = userMapper.selectUserByName(qo.getuName());
        if (user == null) {
            return null;
        }
        //验证手机号码和用户名匹配
        if (!user.getuPhone().equals(qo.getuPhone())) {
            return null;
        }
        //验证激活状态
        if (user.getuStatus() == 0) {
            //用户未激活
            //可以抛出异常
            return null;
        }
        return user;
    }

    @Override
    public User userInfoUpData(UserInfoQO qo) {
        User user = userMapper.selectUserByUId(qo.getuId());
        System.out.println("userMapper.upDataUserInfoByQo(qo)+++++++t = "+user);

        user.setuEmail(qo.getuEmail());
        user.setuPhone(qo.getuPhone());
        user.setuSex(qo.getuSex());
        user.setuName(qo.getuName());
        user.setuPassword(qo.getuPassword());
        System.out.println("userMapper.upDataUserInfoByQo(qo)+++++++t = "+user);
        //修改数据
        int result = userMapper.upDataUserInfo(user);

        System.out.println("userMapper.upDataUserInfoByQo(qo);---result = " + result);
        if (result == 0) {
            return null;
        }
        User resultUser = userMapper.selectUserByUId(qo.getuId());
        System.out.println("userInfoUpData----user = " + resultUser);
        return resultUser;
    }

    @Override
    public String logoutUser(UserLogoutQO qo) {

        //判断用户名和电话是否匹配
        User user = userMapper.selectUserByUId(qo.getuId());
        if (!user.getuName().equals(qo.getuName())) {
            return "名字不匹配";
        }
        if (!user.getuPhone().equals(qo.getuPhone())) {
            return "电话不匹配";
        }
        //删除用户
        int result = userMapper.deleteUserByUId(qo.getuId());
        //返回结果
        if (result == 0) {
            return "删除失败！";
        }
        return "删除成功！";
    }
}
