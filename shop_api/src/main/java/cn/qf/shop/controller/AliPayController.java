package cn.qf.shop.controller;


import cn.qf.shop.alipay.config.AlipayConfig;
import cn.qf.shop.pojo.entity.Pay;
import cn.qf.shop.pojo.qo.OrdersQO;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

/**
 * @author cxf
 */
@Controller
@RequestMapping("/aliPay")
@Api(tags = "支付宝支付接口")
public class AliPayController extends BaseController {
    /**
     * 支付宝支付的方法
     */
    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    @ApiOperation(value = "支付方法")
    public void pay(@RequestBody OrdersQO ordersQO , HttpServletRequest request, HttpServletResponse resp) {

        //打印获取到的前端请求参数（1.订单编号oId 2.订单总金额oCount）
        String oId = ordersQO.getoId();
        /**
         * //从Bigdecimal类型转换为Double类型
         */
        Double oCount = ordersQO.getoCount().doubleValue();
        System.out.println("获取到的订单编号："+oId);
        System.out.println("获取到的订单总金额："+oCount);


        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        //1. 获取所有的参数
//        Pay pay = (Pay) request.getAttribute("pay");
        Pay pay = new Pay(oId,oCount,"orderTest",null);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        try {
            String out_trade_no = new String(pay.getOutTradeNo().getBytes("ISO-8859-1"),"UTF-8");
            //付款金额，必填
            String total_amount = new String(pay.getTotalAmount().toString().getBytes("ISO-8859-1"),"UTF-8");
            //订单名称，必填
            String subject = new String(pay.getSubject().getBytes("ISO-8859-1"),"UTF-8");
            //商品描述，可空
//            String body = new String(pay.getBody().getBytes("ISO-8859-1"),"UTF-8");
            //超时时间
            String timeout_express = new String(pay.getTimeoutExpress().toString().getBytes("ISO-8859-1"),"UTF-8");

            alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                    + "\"total_amount\":\""+ total_amount +"\","
                    + "\"subject\":\""+ subject +"\","
//                    + "\"timeout_express\":\"" + timeout_express + "m\","
                    + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

            //请求
            String result = alipayClient.pageExecute(alipayRequest).getBody();
            resp.getWriter().write(result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
