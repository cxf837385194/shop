package cn.qf.shop.controller;

import cn.qf.shop.pojo.entity.Address;
import cn.qf.shop.pojo.qo.address.AddressAddQO;
import cn.qf.shop.pojo.qo.address.AddressInsertQO;
import cn.qf.shop.pojo.qo.address.AddressUpdateQO;
import cn.qf.shop.pojo.vo.RespResult;
import cn.qf.shop.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/address")
@Api(tags="地址业务接口")
public class AddressController extends BaseController{

    @Autowired
    private AddressService addressService;


    /**
     * 根据用户ID获取用户地址
     * @param addressAddQO 参数实体 传递用户ID
     * @return
     */
    @RequestMapping(value = "/infoPost", method = RequestMethod.POST)
    @ApiOperation("根据用户ID获取用户地址")
    public RespResult info(@RequestBody AddressAddQO addressAddQO){
        System.out.println("info"+addressAddQO.getuId());
//        List<Address> address = addressService.selectAddressByUid(addressAddQO);
        List<Address> address = addressService.selectAddressByQO(addressAddQO);

        return returnRespWithSuccess(address);
    }

    /**
     * 根据用户ID获取用户地址
     * @param uId
     * @return
     */
    @GetMapping("/info")
    @ApiOperation("根据用户ID获取用户地址")
    @ApiImplicitParam(paramType ="query",name="uId",value = "用户id",required=true,dataType = "Integer")
    public RespResult info(Integer uId){
        System.out.println("info"+uId);
        List<Address> address = addressService.selectAddressByUid(uId);
        return returnRespWithSuccess(address);
    }

    /**
     * 根据用户ID添加地址
     *
     * @param
     * @return
     */
    @PostMapping("/insertAddress")
    @ApiOperation("根据用户ID添加地址")
    /*   @ApiImplicitParam(paramType ="query",name="uId",value = "用户id",required=true,dataType = "Integer")*/
    public RespResult insertAddress(@RequestBody AddressAddQO addressAddQO){
        System.out.println("info");
        int i = addressService.insertAddress(addressAddQO);
        return returnRespWithSuccess(i);
    }


    /**
     * 根据用户id来修改地址
     * @param addressUpdateQO
     * @return
     */
    @PostMapping("/updateAddress")
    @ApiOperation("修改用户地址")
    public RespResult updateAddress(@RequestBody AddressUpdateQO addressUpdateQO){
        System.out.println("update");
        System.out.println(addressUpdateQO);
        int i=addressService.updateByAid(addressUpdateQO);
        return returnRespWithSuccess(i  );
    }


    /**
     * 根据地址Aid来查询地址信息
     * @param address
     * @return
     */
    @RequestMapping(value = "/findAddressByAid", method = RequestMethod.POST)
    @ApiOperation(value = "根据地址Aid查询地址信息")
    public RespResult selectAddressByAid(@RequestBody Address address){

        Address addressByAid = addressService.selectAddressByAid(address);

        return returnRespWithSuccess(addressByAid);

    }

    /**
     * 根据用户ID添加地址
     *
     * @param
     * @return
     * @author xkh
     */
    @PostMapping("/insertAddressPost")
    @ApiOperation("添加地址")
    public RespResult insertAddressPost(@RequestBody AddressInsertQO addressAddQO){
        System.out.println(addressAddQO);
        int i = addressService.insertAddressPost(addressAddQO);
        if (i == 0) {
            return returnRespWithFailed("添加失败");
        }
        return returnRespWithSuccess("添加成功！");
    }

    @PostMapping("/deleteAddress")
    @ApiOperation("根据地址AID从购物车来删除地址")
    @ApiImplicitParam(paramType ="query",name = "aId", value="地址ID",required = true,dataType = "Integer")
    public RespResult deleteArByAid(@RequestBody Address qo){
        System.out.println("----------"+qo);

        int i= addressService.deleteArByAid(qo.getaId());
        return returnRespWithSuccess(i);
    }

}