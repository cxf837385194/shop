package cn.qf.shop.controller;

import cn.qf.shop.pojo.entity.User;
import cn.qf.shop.pojo.qo.user.UserInfoQO;
import cn.qf.shop.pojo.qo.user.UserLoginQO;
import cn.qf.shop.pojo.qo.user.UserLogoutQO;
import cn.qf.shop.pojo.qo.user.UserRegisterQO;
import cn.qf.shop.pojo.vo.RespResult;
import cn.qf.shop.service.UserService;
import cn.qf.shop.utils.Base64Utils;
import cn.qf.shop.utils.SendSmsUtils;
import com.google.code.kaptcha.Constants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(tags = "用户业务接口")
public class UserController extends BaseController{

    @Autowired
    UserService userService;

    @RequestMapping(value = "/register" , method = RequestMethod.POST)
    @ApiOperation("用户注册")
    public RespResult register(@RequestBody UserRegisterQO qo, HttpServletRequest request) {

        System.out.println("qo = " + qo);
        User user = userService.registerUser(qo);

        return returnRespWithSuccess(user);
    }

    @GetMapping("/activate")
    @ApiOperation("用户码激活")
    public RespResult activate(String returnCode) {
        //解码code
        String code = Base64Utils.decode(returnCode);
        //调用服务查询code的用户
        User user = userService.activateUser(code);
        //没有user激活失败  -1
        if (user == null) {
            return returnRespWithFailed("激活失败");
        }
        //有user说明激活成功  0
        return returnRespWithSuccess(user,"激活成功！");
    }

    /**
     * 暂时没用到...
     */
    @GetMapping("/verifyActive")
    @ApiOperation("确认是否激活")
    public RespResult verifyActive(String uName) {
        User user = userService.verifyActive(uName);
        if (user == null) {
            returnRespWithFailed("该用户还没激活呢");
        }
        return returnRespWithSuccess(user,"该用户已经激活了");
    }


    @PostMapping(value = "/passwordLogin")
    @ApiOperation("账号密码登录")
    public RespResult passwordLogin(@RequestBody UserLoginQO qo, HttpServletRequest request, HttpServletResponse response) {
        //获取session中的图片验证码
        String vcode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if (!vcode.equalsIgnoreCase(qo.getVcode())) {
            return returnRespWithFailed("图片验证码错误！");
        }
        System.out.println("执行验证码："+qo);
        User user = userService.loginUser(qo);
        if (user == null) {
            return returnRespWithFailed("登录失败");
        }
        //验证成功返回user信息存入session中
        request.getSession().setAttribute("loginUser", user);
        return returnRespWithSuccess(user);
    }

    @PostMapping(value = "/smsLogin")
    @ApiOperation("短信用户登录")
    public RespResult msmLogin(@RequestBody UserLoginQO qo, HttpServletRequest request, HttpServletResponse response) {
        //获取session中的短信验证码
        String smsCode = (String) request.getSession().getAttribute("smsCode");
        if (!smsCode.equalsIgnoreCase(qo.getNoteCode())) {
            return returnRespWithFailed("短信验证码错误！");
        }
        User user = userService.smsLoginUser(qo);
        if (user == null) {
            return returnRespWithFailed("登录失败，请检查电话和激活状态");
        }
        //验证成功返回user信息存入session中
        request.getSession().setAttribute("loginUser", user);
        return returnRespWithSuccess(user);
    }

    @PostMapping(value = "/submitSms")
    @ApiOperation("发送短信")
    public RespResult submitSms(@RequestBody UserLoginQO qo, HttpServletRequest request, HttpServletResponse response) {

        //去service中验证手机号和用户名是否匹配
        User user = userService.checkTelByUserName(qo);
        if (user == null) {
            return returnRespWithFailed("电话号码与用户名不匹配");
        }
        //随机生成四位数字验证码
        String smsCode = SendSmsUtils.getRandCheckCode(4);
        //发送短信
        SendSmsUtils.sendSms(user.getuPhone(), smsCode);
        //将smsCode存入session中
        request.getSession().setAttribute("smsCode", smsCode);
        return returnRespWithSuccess("短信发送成功，请用户填写验证码");
    }

    /**
     * 模拟发送短信，验证码在log台看
     */
    @PostMapping(value = "/submitSmsNo")
    public RespResult submitSmsNo(@RequestBody UserLoginQO qo, HttpServletRequest request, HttpServletResponse response) {

        //随机生成四位数字验证码
        String smsCode = SendSmsUtils.getRandCheckCode(4);
        //将smsCode存入session中
        request.getSession().setAttribute("smsCode", smsCode);

        System.out.println("*******模拟发送短信*******");
        System.out.println("smsCode："+smsCode);

        return returnRespWithSuccess("短信发送成功，请用户填写验证码："+smsCode);
    }

    @PostMapping(value = "/putUserToSession")
    @ApiOperation("检测到前端自动登录，将用户存入后台session中")
    public RespResult putUserToSession(@RequestBody UserLoginQO qo, HttpServletRequest request, HttpServletResponse response) {

        User user = (User) request.getSession().getAttribute("loginUser");
        if (user != null) {
            return returnRespWithSuccess("已经有了");
        }
        user = userService.loginUser(qo);
        if (user == null) {
            return returnRespWithFailed("登录失败");
        }
        //验证成功返回user信息存入session中
        request.getSession().setAttribute("loginUser", user);
        return returnRespWithSuccess("存入后台session成功！"+user);
    }

    @PostMapping(value = "/userInfoUpData")
    @ApiOperation("更新用户基本数据")
    public RespResult userInfoUpData(@RequestBody UserInfoQO qo, HttpServletRequest request) {

        System.out.println("qo = " + qo);
        //调用userService 更新用户数据
        User user = userService.userInfoUpData(qo);

        if (user == null) {
            return returnRespWithFailed("修改失败");
        }

        //更新成功更新session的loginUser
        request.getSession().setAttribute("loginUser",user);

        return returnRespWithSuccess(user);
    }

    @PostMapping(value = "/logoutUser")
    @ApiOperation("注销用户")
    public RespResult logoutUser(@RequestBody UserLogoutQO qo, HttpServletRequest request) {

        System.out.println("qo = " + qo);

        //获取session中的短信验证码
        String smsCode = (String) request.getSession().getAttribute("smsCode");
        if (!smsCode.equalsIgnoreCase(qo.getNoteCode())) {
            return returnRespWithFailed("短信验证码错误！");
        }

        //判断用户名和密码
        String result = userService.logoutUser(qo);
        System.out.println("result = " + result);
        if (!result.equals("删除成功！")) {
            return returnRespWithFailed(result);
        }

        return returnRespWithSuccess(result);
    }
}
