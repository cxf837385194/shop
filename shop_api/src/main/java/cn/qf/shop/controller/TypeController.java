package cn.qf.shop.controller;


import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.entity.Type;
import cn.qf.shop.pojo.qo.goods.GoodsQO;
import cn.qf.shop.pojo.vo.RespResult;
import cn.qf.shop.service.TypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/type")
@Api(tags = "商品类别业务接口")
public class TypeController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(TypeController.class);

    @Autowired
    private TypeService typeService;


    @GetMapping("/findAll")
    @ApiOperation("获取所有商品类别")
    public RespResult findAll(){
        logger.info("findAll");
        List<Type> types = typeService.findAll();
        return returnRespWithSuccess(types);

    }


    @RequestMapping(value = "/findTypeByTid", method = RequestMethod.POST)
    @ApiOperation(value = "根据商品类别ID查询商品类别信息")
    public RespResult selectByTid(@RequestBody Type type){

        Type typeId = typeService.selectByTid(type);

        logger.info("msg:{}",typeId);
        return returnRespWithSuccess(typeId);

    }
}
