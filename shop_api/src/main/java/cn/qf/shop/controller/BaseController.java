package cn.qf.shop.controller;

import cn.qf.shop.pojo.vo.RespResult;

/**
 * Controller 父类
 */
public class BaseController {
    //请求处理成功，返回业务数据
    RespResult returnRespWithSuccess(Object data) {
        RespResult respResult = new RespResult();
        respResult.setCode(RespResult.Code.SUCCESS.getValue());
        respResult.setData(data);
        return respResult;
    }

    /**
     * 方法重载 可以传信息
     */
    //请求处理成功，返回业务数据
    RespResult returnRespWithSuccess(Object data,String message) {
        RespResult respResult = new RespResult();
        respResult.setCode(RespResult.Code.SUCCESS.getValue());
        respResult.setData(data);
        respResult.setMessage(message);
        return respResult;
    }

    //请求处理失败，返回提示信息
    RespResult returnRespWithFailed(String message) {
        RespResult respResult = new RespResult();
        respResult.setCode(RespResult.Code.FAILED.getValue());
        respResult.setMessage(message);
        return respResult;
    }
}
