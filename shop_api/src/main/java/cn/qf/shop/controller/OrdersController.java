package cn.qf.shop.controller;


import cn.qf.shop.pojo.entity.Cart;
import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.entity.Orders;
import cn.qf.shop.pojo.qo.OrdersQO;
import cn.qf.shop.pojo.qo.goods.GoodsQO;
import cn.qf.shop.pojo.vo.RespResult;
import cn.qf.shop.service.GoodsService;
import cn.qf.shop.service.OrdersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;


@RestController
@Controller
@RequestMapping("/orders")
@Api(tags = "订单业务接口")
public class OrdersController extends BaseController{

    /**
     * 需要输出日志的类，可以创建一个log属性
     */
    private Logger logger = LoggerFactory.getLogger(OrdersController.class);


    /**
     * 初始化订单业务逻辑层对象
     */
    @Resource
    private OrdersService ordersService ;


    /**
     * 初始化商品业务逻辑层对象
     */
    @Resource
    private GoodsService goodsService ;


    @RequestMapping(value = "/showAllOrders", method = RequestMethod.POST)
    @ApiOperation(value = "获取所有订单信息")
    public RespResult showAllOrders(@RequestBody OrdersQO ordersQO){
        System.out.println("showAllOrders");
        System.out.println("获取到的用户ID："+ordersQO.getuId());
        logger.debug("获取所有订单信息");
        List<Orders> ordersList = ordersService.selectAllOrders(ordersQO);
        logger.debug("查询到的数据:{}",ordersList);
        return returnRespWithSuccess(ordersList) ;
    }

    @RequestMapping(value = "/findOrderByOid", method = RequestMethod.POST)
    @ApiOperation(value = "根据订单号查询订单")
    public Orders findOrderByOid(@RequestBody OrdersQO ordersQO){
        logger.debug("根据订单号查询订单");
        System.out.println(ordersQO);
        System.out.println("获取到的用户ID："+ordersQO.getuId());
        System.out.println("获取到的订单编号："+ordersQO.getoId());
        Orders orders = ordersService.selectByOid(ordersQO);
        logger.debug("查询到的数据:{}",orders);
        System.out.println(orders);
        return orders ;
    }

    @RequestMapping(value = "/findOrderByOstate", method = RequestMethod.POST)
    @ApiOperation(value = "根据订单状态查询订单")
    public RespResult findOrderByOstate(@RequestBody OrdersQO ordersQO){
        logger.debug("根据订单状态查询订单");
        List<Orders> ordersList = ordersService.selectByOstate(ordersQO);
        logger.debug("查询到的数据:{}",ordersList);
        return returnRespWithSuccess(ordersList) ;
    }

    @RequestMapping(value = "/addAssessByOid", method = RequestMethod.POST)
    @ApiOperation(value = "增加订单评价")
    public Orders addAssessByOid(@RequestBody OrdersQO ordersQO){
        logger.debug("增加订单评价");
        ordersService.updateAssessByOid(ordersQO);
        logger.debug("查询增加评价的订单信息");
        Orders orders = ordersService.selectByOid(ordersQO);
        logger.debug("查询到的数据:{}",orders);
        System.out.println(orders);
        return orders ;
    }


    @RequestMapping(value = "/deleteOrderByOid", method = RequestMethod.POST)
    @ApiOperation(value = "删除订单")
    public void deleteOrderByOid(@RequestBody OrdersQO ordersQO){
        logger.debug("删除订单");
        ordersService.deleteOrderByOid(ordersQO);

    }



    @RequestMapping(value = "/addOrderByOid", method = RequestMethod.POST)
    @ApiOperation(value = "新增订单")
    public void addOrderByOid(@RequestBody OrdersQO ordersQO){
        logger.debug("新增订单");


        System.out.println("获取到的用户ID："+ordersQO.getuId());
        System.out.println("获取到的地址ID："+ordersQO.getaId());
        System.out.println("获取到的总金额："+ordersQO.getoCount());

        ordersService.addOrderByOid(ordersQO);
        //1.前端发送请求的参数（1.地址ID 2.用户ID 3.购物车的总金额）

        /*1.1 使用地址ID关联地址表 ，
        查询 1.收件人（aName）
             2.电话号码（aPhone）
             3. 详细地址（aDetail）
        */


        /*1.2 使用用户ID关联购物车表 ，
        * 查询：
        *       1.商品ID  通过商品ID查询商品的图片、描述、上市时间、名称、单价
        *       2.购物车购买单件商品的数量
        *       3.商品的小计
        * */

        /*
        * 1.3  获取到的购物车总金额直接写入订单表中
        *       作为订单的总金额  oCount
        * */
    }


    @RequestMapping(value = "/selectGoodsByGid", method = RequestMethod.POST)
    @ApiOperation(value = "根据商品ID查询数据")
    public RespResult selectGoodsByGid(@RequestBody GoodsQO goodsQO){
        logger.info("根据商品ID查询购物车表中的商品信息");


        ordersService.selectGoodsByGid(goodsQO);

        return returnRespWithSuccess("执行成功！！！") ;
    }


//    chageOstateTwo


    @RequestMapping(value = "/chageOstateTwo", method = RequestMethod.POST)
    @ApiOperation(value = "发货")
    public RespResult chageOstateTwo(@RequestBody OrdersQO ordersQO){
        logger.info("发货");
        ordersService.chageOstateTwo(ordersQO);
        return returnRespWithSuccess("执行成功！！！") ;
    }


//    chageOstateFour


    @RequestMapping(value = "/chageOstateFour", method = RequestMethod.POST)
    @ApiOperation(value = "收货")
    public RespResult chageOstateFour(@RequestBody OrdersQO ordersQO){
        logger.info("收货");
        ordersService.chageOstateFour(ordersQO);
        return returnRespWithSuccess("执行成功！！！") ;
    }
}
