package cn.qf.shop.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;


/**
 * 支付宝
 * 支付测试servlet
 */
@WebServlet(value = "/testPay")
public class TestServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.请求和响应 统一编码
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        //2.获取前端发送请求过来的参数   （1.订单编号 2.订单的总金额）
        String oId = request.getParameter("oId");
        String oCount = request.getParameter("oCount");

        System.out.println("获取到的订单编号：" + oId);
        System.out.println("获取到的订单的总金额：" + oCount);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
