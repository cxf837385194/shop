package cn.qf.shop.controller;

import cn.qf.shop.pojo.vo.RespResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * 用于上传商品图片到img文件
 */

@Api(tags = "文件操作")
@RestController
public class FileController {

    @ApiOperation("文件上传")
    @PostMapping(value = "/upload", consumes = "multipart/*",
            headers = "content-type=multipart/form-data")
    public RespResult upload(@ApiParam("上传文件") MultipartFile file, HttpServletRequest request){


        if (file.isEmpty()){
            //文件为空
            throw new RuntimeException("文件上传失败");

        }


        String savePath="D:/JavaTest/part3/shop/view/shop-app/goods/img/"+file.getOriginalFilename();
        System.out.println("upload:"+savePath);
        System.out.println("url:http://localhost:8080/JavaTest/part3/shop/view/shop-app/goods/img/"+file.getOriginalFilename());
        try {
            file.transferTo(new File(savePath));
        } catch (IOException e) {
            throw new RuntimeException("文件上传失败");
        }

        //文件上传成功
        return new RespResult();


    }

}
