package cn.qf.shop.controller;

import cn.qf.shop.mapper.CartMapper;
import cn.qf.shop.mapper.GoodsMapper;
import cn.qf.shop.pojo.entity.Cart;
import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.vo.RespResult;
import cn.qf.shop.service.CartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/cart")
@Api(tags = "购物车业务接口")
public class CartController extends BaseController{

    @Autowired
    private CartService cartService;

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private CartMapper cartMapper;

    /**
     * 根据商品ip查询购物车数据
     */
    @GetMapping("/selectCartByCid")
    @ApiOperation("根据商品id查询购物车消息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "uId", value = "用户名"
                    , required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "gId", value = "商品id"
                    , required = true, dataType = "Integer")
    })
    public RespResult selectCartByCid(Integer uId, Integer gId){
        Cart cart = cartService.selectCartByCid(uId,gId);
        if(cart==null){
            int i = cartService.insertCart(uId, gId);
            System.out.println("CartService"+i);
            return returnRespWithSuccess(i);
        }else{
            Integer cNum = cart.getcNum();
            Integer cId = cart.getcId();
            Goods goods = goodsMapper.selectGoodsById(gId);
            BigDecimal price = goods.getgPrice();
            BigDecimal bigCnum = new BigDecimal(cNum+1);
            BigDecimal cCount = bigCnum.multiply(price);
            int i = cartMapper.updateCartByCid(cId,cNum+1,cCount);
            if(i==0){
                throw new RuntimeException("cId不存在");
            }
            return returnRespWithSuccess(i);
        }
    }


    /**
     * 根据用户id查询显示购物车数据
     * @param userId
     * @return
     */
    @GetMapping("/selectCart")
    @ApiOperation(value="根据用户id查询显示购物车数据")
    @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID"
            , required = true, dataType = "long")
    public RespResult selectCartByUserId(Long userId){
        System.out.println(userId);
        List<Cart> cart = cartService.selectCartByUserId(userId);
        System.out.println("1111");
        return returnRespWithSuccess(cart);
    }


    /**
     * 添加商品到购物车通过用户id和商品id
     * @param uId
     * @param gId
     * @return
     */
    @GetMapping("/insertCart")
    @ApiOperation("根据用户id和商品id添加商品到购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "uId", value = "用户名"
                    , required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "gId", value = "商品id"
                    , required = true, dataType = "Integer")
    })
    public RespResult insertCart(Integer uId, Integer gId){
        int i = cartService.insertCart(uId, gId);
        return returnRespWithSuccess(i);
    }


    /**
     * 根据购物车id从购物车删除商品
     * @param cId
     * @return
     */
    @GetMapping("/deleteCart")
    @ApiOperation("根据购物车id从购物车删除商品")
    @ApiImplicitParam(paramType = "query", name = "cId", value = "购物车ID"
            , required = true, dataType = "Integer")
    public RespResult deleteCartByCid(Integer cId){
        int i = cartService.deleteCartByCid(cId);
        return returnRespWithSuccess(i);
    }

    /**
     * 根据用户id清空该用户的购物车
     * @param userId
     * @return
     */
    @GetMapping("/clearCart")
    @ApiOperation("根据用户id清空该用户的购物车")
    @ApiImplicitParam(paramType = "query", name = "userId", value = "用户ID"
            , required = true, dataType = "Integer")
    public RespResult clearCart(Integer userId){
        int i = cartService.clearCart(userId);
        return returnRespWithSuccess(i);
    }

    /**
     * 修改商品的数量及小计
     * @param cId
     * @param cNum
     * @param gPrice
     * @return
     */
    @GetMapping("/updateCart")
    @ApiOperation("修改商品的数量及小计")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "cId", value = "购物车Id"
                    , required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "cNum", value = "商品数量"
                    , required = true, dataType = "Integer"),
            @ApiImplicitParam(paramType = "query", name = "gPrice", value = "商品单价"
                    , required = true, dataType = "Integer")
    })
    public RespResult updateCartByCid(Integer cId,Integer cNum,Integer gPrice){
        BigDecimal gPrice1 = new BigDecimal(gPrice);
        int i = cartService.updateCartByCid(cId,cNum,gPrice1);
        return returnRespWithSuccess(i);
    }






}
