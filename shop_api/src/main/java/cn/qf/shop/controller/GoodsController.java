package cn.qf.shop.controller;

import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.qo.goods.GoodsQO;
import cn.qf.shop.pojo.vo.RespResult;
import cn.qf.shop.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/goods")
@Api(tags = "商品业务接口")
public class GoodsController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(GoodsController.class);

    @Autowired
    private GoodsService goodsService;

    @GetMapping("listAll")
    @ApiOperation("获取所有商品")
    public RespResult listAll(){
        logger.info("listAll");
        List<Goods> goods = goodsService.listAll();
        return returnRespWithSuccess(goods);

    }

    @RequestMapping(value = "/findGoodsByGid", method = RequestMethod.POST)
    @ApiOperation(value = "根据商品Gid查询商品信息")
    public RespResult selectByGId(@RequestBody GoodsQO goodsQO){

        Goods goods = goodsService.selectByGId(goodsQO);
        logger.info("msg:{}",goods);
        return returnRespWithSuccess(goods);

    }


    @RequestMapping(value = "/findGoodsByTid", method = RequestMethod.POST)
    @ApiOperation(value = "根据商品类别Tid查询商品信息")
    public RespResult selectByTid(@RequestBody Goods goods){

        List<Goods> goodsList = goodsService.selectByTid(goods);
        logger.info("msg:{}",goodsList);
        return returnRespWithSuccess(goodsList);

    }



}
