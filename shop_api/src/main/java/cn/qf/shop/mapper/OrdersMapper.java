package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Cart;
import cn.qf.shop.pojo.entity.Orders;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单模块数据访问层接口
 */
public interface OrdersMapper {

    /**
     * 查询所有订单数据，
     * 用于订单页面数据展示
     * @return 从数据库中查询到的所有订单信息
     */
    List<Orders> selectAllOrders(@Param("uId") Integer uId);

    /**
     * 根据订单编号查询订单数据
     * @param oId 订单编号
     * @return 从数据库中查询到的订单数据
     */
    Orders selectByOid(@Param("uId")Integer uId ,@Param("oId") String oId);

    /**
     * 修改订单的状态
     * @param oId 订单编号
     */
    void updateStateByOid(@Param("oId") String oId);


    /**
     * 根据订单状态查询
     * @param oState 订单状态
     * @return 订单信息
     */
    List<Orders> selectByOstate(@Param("uId")Integer uId ,@Param("oState") Integer oState);

    /**
     * 定点订单编号修改订单评价
     * @param oId  订单编号
     * @param oAssess 订单评价
     */
    void updateAssessByOid(@Param("oId") String oId , @Param("oAssess") String oAssess );

    /**
     * 定点订单编号删除订单
     * @param oId 订单编号
     */
    void deleteOrderByOid(@Param("oId") String oId);


    /**
     * 添加订单
     * @param aId   //地址主键 用于关联地址表查询
     * @param uId   //用户主键 用于关联购物车表查询
     * @param Count  //购物车提交的总金额 ，即订单的总金额
     */
    void addOrderByOid(@Param("uId") Integer uId ,@Param("aId") Integer aId , @Param("oId") String oId , @Param("Count") BigDecimal Count);



    /**
     * 根据商品ID在购物车表中
     * 查询商品信息
     * @param gId  参数： 商品ID （oId）
     * @return  true（存在数据）  false（不存在数据）
     */
    Cart selectGoodsByGid(@Param("gId") Integer gId );


    /**
     * 更新购物车表中的商品小计和数量
     * @param count 1.count(商品小计)
     * @param num  2.num(商品的数量)
     */
    void upCartNumAndCartCountByOid(@Param("gId") Integer gId ,@Param("cCount") BigDecimal count, @Param("cNum") int num);

    /**
     * 向购物车表中第一次写入商品信息
     * @param uId  用户ID
     * @param gId  商品ID
     * @param gPrice 商品单价
     */
    void addGoodsToCart(@Param("uId") Integer uId, @Param("gId")Integer gId, @Param("gPrice") BigDecimal gPrice);


    /**
     * 根据地址ID连表查询
     * @param aId
     * @return
     */
    Orders selectAddressByAid(Integer aId);

    /**
     * 根据订单编号查询订单信息
     * 修改订单状态 从0（待支付状态） 改为 1（已支付，待发货）
     * @param oId
     */
    void updatePayStatus(@Param("oId") String oId);

    /**
     * 发货
     * @param oId
     */
    void chageOstateTwo(@Param("oId") String oId);

    /**
     * 收货
     * @param getoId
     */
    void chageOstateFour(@Param("oId")String getoId);
}
