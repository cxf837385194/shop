package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.User;
import cn.qf.shop.pojo.qo.user.UserInfoQO;

public interface UserMapper {
    /**
     * 新增用户
     * @param user
     * @return
     */
    int insertUser(User user);

    /**
     * 根据用户名 查询用户
     * @param uName
     * @return
     */
    User selectUserByName(String uName);

    /**
     * 根据用户码查询用户
     * @param code
     * @return
     */
    User selectUserByCode(String code);

    /**
     * 更新用户数据
     * @param user
     * @return
     */
    int updateUserByUId(User user);

    /**
     * 更新用户基本信息
     *
     * @param
     * @return
     */
    int upDataUserInfo(User user);

    /**
     * 根据id查询用户
     * @param uId
     * @return
     */
    User selectUserByUId(Integer uId);

    /**
     * 删除用户
     * @param uId
     * @return
     */
    int deleteUserByUId(Integer uId);
}
