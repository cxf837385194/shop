package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Address;
import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.qo.address.AddressAddQO;
import cn.qf.shop.pojo.qo.address.AddressUpdateQO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {

    List<Address> selectAddressByUid(Integer uId);

    /**
     * 根据用户id创建
     * @param addressAddQO
     * @return
     */
    int insertAddress(AddressAddQO addressAddQO);

    /**
     * 假删除
     * @param aId
     * @return
     */
    int deleteArByAid(Integer aId);

    /**
     * 用aid来更新信息
     * @param addressAddQO
     * @return
     */
    int updateByAid(AddressAddQO addressAddQO);


    /**
     * 根据地址aId查询地址信息
     * @param aId   地址Id
     * @return  从数据库查询地址信息
     */
    Address selectByAId(@Param("aId") Integer aId);

    int upDataByUpdateQO(AddressUpdateQO addressUpdateQO);

    int insertAddressPost(Address address);
}