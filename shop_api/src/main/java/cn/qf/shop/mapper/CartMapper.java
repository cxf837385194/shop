package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Cart;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface CartMapper {

    /**
     * 通过用户id查询对应的购物车数据
     * @param userId
     * @return
     */
    List<Cart> selectCartByUserId(Long userId);

    /**
     * 通过用户id和商品id添加商品到用户的购物车
     * 主要传两个参数需加注解@Param
     * @param
     * @param
     * @return
     */
    int insertCart(Cart cart);

    /**
     * 通过购物车id删除购物车的商品
     * @param cId
     * @return
     */
    int deleteCartByCid(Integer cId);

    /**
     * 根据用户id删除该用户购物车数据
     * @param userId
     * @return
     */
    int clearCart(Integer userId);


    /**
     * 修改商品的数量
     * @param cId
     * @param cNum
     * @param cCount
     * @return
     */
    int updateCartByCid(@Param("cId") Integer cId,
                        @Param("cNum") Integer cNum,
                        @Param("cCount") BigDecimal cCount);

    /**
     * 根据用户id和商品id查询数据库
     * @param uId
     * @param gId
     * @return
     */
    Cart selectCartByCid(@Param("uId")Integer uId,
                         @Param("gId") Integer gId);
}
