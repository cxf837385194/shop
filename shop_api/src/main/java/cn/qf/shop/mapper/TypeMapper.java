package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Goods;
import cn.qf.shop.pojo.entity.Type;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品类别数据操作接口
 */
public interface TypeMapper {



    /**
     * 获取所有商品类别列表
     * @return
     */
    List<Type> selectTypeAll();




    /**
     * 根据商品类别ID 查询商品类别数据
     *
     * @param tId  商品类别id
     * @return  从数据库中查询到商品类别数据
     */
    Type selectTypeById(@Param("tId") int tId);

}
