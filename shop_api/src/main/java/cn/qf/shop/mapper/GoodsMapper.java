package cn.qf.shop.mapper;


import cn.qf.shop.pojo.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品数据操作接口
 */
public interface GoodsMapper {


    /**
     * 获取所有商品列表
     * @return
     */
    List<Goods> selectAll();



    /**
     * 根据商品gid查询商品数据
     * @param gId 商品id
     * @return  从数据库中查询到商品数据
     */

    Goods selectGoodsById(@Param("gId") int gId);


    /**
     * 根据商品类别tid查询商品数据
     * @param tId 商品id
     * @return  从数据库中查询到商品数据
     */

    List<Goods> selectGoodsByTid(@Param("tId") int tId);



}
