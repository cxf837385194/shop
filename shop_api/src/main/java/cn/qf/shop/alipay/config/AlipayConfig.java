package cn.qf.shop.alipay.config;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2021000119647893";

	// 商户私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDDirSO/GHKP7az9L+4S8MsZblFVJkWEvLXUX67M0egY2BjMFuH14v1nUwJgbkSnZPn2WisqoH0hP8gFXHayNCZIPH0xw/qlCbbgXhoL5plb+A3/3AWyKRfob8maj6OSic0tdyz8KzzqYWG3pjwLX4O6c60+Ob88EZXXncJq+BWJyEoiGRweleiWJ3MX8HEyz8jDKy/VwyOf1kLYUgnUjfQL6lDpQCoyQGPjWeBqN7AvK68nCqzxyBC60n0Asmozajp3RnxLBCoibSUlRAsUUgACUZcaRpqI8WHWBI6uV5STfiWXj7CF/1/iwsbZolmgkf9QytafeWO8KegyP/tswR7AgMBAAECggEAM4pJ0S5gsChfBcvLBM6jYP5m+Qk1WIYiD/LhDoKTrIjFDR/f4ExGDbUqJ49Tsr5qD8BUQL6ohkTP4isE2X/4y4PNgpE9dr0aCN11WTDC5123ocroRnkYKkS912hFmMee0dvlkmArlgXmtgfSE+xJv7CM1VKNv7mtMLYrdhchiFNNkOCmrs/tEY1lcrvHMFUvKD9kMwj7gOAvn8BBslhgv8SdPrLWhy8qvBNuXczgfSeSpy4m1Rj9ftQS5POEFshItgZdPOUTikFOoRMqhGCFybsQeNaQRzYusyDmS+31zq2bTK7hCDVemF3Wk3T1R8uGKCvHfo84xbi1rHJflOUiwQKBgQD1DSjfKCCTUL1vPmRaAaX7VvV7lI1RW+fkXP+eiMcm986Gu/Q7aSloqY6ahbyOOo6Dnl5N0RM/nr642W2TsbIugwCiYVNSDo7KPRa+t0OXDo5yaoeqFLKo4QmFy3BMHRUT6KKlXz8XU7cFaIQtLDpZRkWcNUHr0sKTwa57fQaQ6wKBgQDMR0RCJ/mOtTgUrVKqmUJCnru/DBBEKnH7x+c6blj83aKYLlrEVFENe8Ns5yCJKmJ0wx3Z/fHBwlqaCjctdEVtFsIZMwoWq+3rhY6RIWyBwLWhaQSLqkO0ciWBfDv5Y8uU4jjzW1eoOkk780TfV/BJtsEZQzCLXyXgVxXVdPX2sQKBgQDt7akRg1SdtHc528pz2cXfE7+mDcZoiGPPRryqoQM8wr6z8ypfxOt/w6HCRredIR3ejZXi2G1oCCoGMJZEIPRwVKH5B6ZutdPOaSH8atQlhuyoCudOLGqLrnQghDa0sEgW3HmPN7t6Wr4JckFj2t2McL6FvPAUmL5z2EohfHMzoQKBgQDBXnDRwu9r1EVa5VrMXLkpQQLt7Oh07kzpjqhmWlBoRREFx1NqZ3o2LkKmkv7seK+sYYqfcNBpnqWPDT6AC4Ewq4teuT7CiUt1V/7FiQMirB4Cxdtkre7bH/Y65TQMdhgfPim0y3FlsI7kbfpzBNZToOiXFlAcjhvXifgEEGxIcQKBgQCM6eqabo08r95mkYGbFt0VLfckoHd+XDKNjxGpHR2yfrUWM7bPO5nmLDoiZ7oNYQtWgvjRgDEYUtm9x19Z/povJnTwcbaCmoAHMBEyiwq11JoSwa0VJIRWi92lol4zzYIpxi9fhFr35t58bewZzVW9CM2uyP15+TlIx2atNuZZIA==" ;

	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
	public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiIwO9hGXAfmZQdhNOuIRPfo60mpLN+XbWyMhVAVd3+5M2PgDOm/clSGtCFV1Moc5MyaOok/x9oIt60pbm2PBSZGVIiXzFX2v7XO5lUub0TRC98f7znCv+7QNC2+Jtki2/bdHA/Ohei7IzTQ5NGMvz7c0vXvGzQ8+vA29MaiZhiJ3qR7++ECm3BRGtpcjiaEkIzFjrSrGoJ52ict45UL9Y5JMRNpe1zbIWr2PFWdm1Zo/vg6X1/IYICgz42z9wecUvKC0ytp0Egep8RLPVjPhZJ0sPvEZ53ST6Idrz3Twhx+Rtsqmxw46v5Po4zdW2g31C43Sa8wu4qV4Urh2AhTklwIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://c3n3940583.qicp.vip/shop-api/callBack/asynchronousAdvice";
//    public static String notify_url = "http://zw.free.idcfengye.com/BeforeStore/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://c3n3940583.qicp.vip/shop-api/callBack/synchronizationAdvice";
//	public static String return_url = "http://zw.free.idcfengye.com/BeforeStore/return_url.jsp";

	// 签名方式
	public static String sign_type = "RSA2";

	// 字符编码格式
	public static String charset = "utf-8";

	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

	// 打印log的文件路径
	public static String log_path = "F:\\log";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	/**
	 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
	 * @param sWord 要写入日志里的文本内容
	 */
	public static void logResult(String sWord) {
		FileWriter writer = null;
		try {
			writer = new FileWriter(log_path + "BeforeStore_alipay_log_" + System.currentTimeMillis()+".txt");
			writer.write(sWord);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}































