package cn.qf.shop.exceptions;

import cn.qf.shop.pojo.vo.RespResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 增强类
 * 全局异常捕获
 */
@ControllerAdvice
@RestControllerAdvice
public class ShopExceptionHandler {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public RespResult handlerException(Exception e) {
        RespResult respResult = new RespResult();
        respResult.setCode(RespResult.Code.FAILED.getValue());
        respResult.setMessage(e.getMessage());
        return respResult;
    }
}
