package cn.qf.shop.utils;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
/*
pom.xml
<dependency>
  <groupId>com.aliyun</groupId>
  <artifactId>aliyun-java-sdk-core</artifactId>
  <version>4.5.3</version>
</dependency>
*/


public class SendSmsUtils {
    /**
     * 产生随机验证码
     * @param len 生成几位验证码
     * @return 返回验证码字符串
     */
    public static String getRandCheckCode(int len) {
        String[] beforeShuffle = new String[]{"2", "3", "4", "5", "6", "7",
                "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z"};
        List list = Arrays.asList(beforeShuffle); //将数组转成List
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
        }
        String afterShuffle = sb.toString();
        String result = afterShuffle.substring(0, len);
        System.out.print(result);
        return result;
    }

    private static final String ACCESS_KEY_Id = "LTAI4G2deoEjtmCzbmQPPCv3";
    private static final String ACCESS_KEY_SECRET = "0Jxky6CC6nXe1X7HYJVIvy9qWEXqxt";
    private static final String REGION_ID = "cn-hangzhou";
    private static final String SIGN_NAME = "shop商城";
    private static final String TEMPLATE_CODE = "SMS_200702910";

    /**
     * 发送短信静态工具类
     * @param telephone 用户电话
     * @param code  验证码
     */
    public static void sendSms(String telephone, String code ) {

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_KEY_Id, ACCESS_KEY_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");

        request.putQueryParameter("RegionId", "cn-hangzhou");
        //电话号码
        request.putQueryParameter("PhoneNumbers", telephone);
        //签名
        request.putQueryParameter("SignName", SIGN_NAME);
        //模板code
        request.putQueryParameter("TemplateCode", TEMPLATE_CODE);
        //验证码
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");


        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试运行的主函数
     * @param args
     */
    public static void main(String[] args) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", ACCESS_KEY_Id, ACCESS_KEY_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        //电话号码
        request.putQueryParameter("PhoneNumbers", "18050530803");
        //签名
        request.putQueryParameter("SignName", SIGN_NAME);
        //模板code
        request.putQueryParameter("TemplateCode", TEMPLATE_CODE);
        //验证码
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + "6666" + "\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}

