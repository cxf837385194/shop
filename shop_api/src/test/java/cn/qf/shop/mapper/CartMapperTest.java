package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Cart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context-mapper.xml"
        , "classpath:spring-druid.xml"
        , "classpath:spring-mybatis.xml"})
public class CartMapperTest {
    //    @Autowired
//    private UserMapper userMapper;
//
//    @Test
//    public void testSelectById() {
//        Long userId = 1L;
//        User user = userMapper.selectById(userId);
//        System.out.println(user);
//    }
    @Autowired
    private  CartMapper cartMapper;

    @Test
    public void testSelecyCartByUserId(){
        List<Cart> cart = cartMapper.selectCartByUserId(1L);
        System.out.println(cart);
    }

    @Test
    public void testInSertCartByUserId(){
        Cart cart = new Cart();
        cart.setcId(1);
        cart.setuId(1);
        cart.setgId(1);
        BigDecimal bDouble = new BigDecimal(100);
        cart.setcCount(bDouble);
        cart.setcNum(1);
        int i = cartMapper.insertCart(cart);
        System.out.println(cart);
        System.out.println("测试"+i);
    }

    @Test
    public void testUpdateCart(){
        Cart cart = new Cart();
        cart.setcId(24);
        cart.setcNum(2);
/*        cart.setuId(1);*/
        BigDecimal bDouble1 = new BigDecimal(200);
        cart.setcCount(bDouble1);
        int i = cartMapper.updateCartByCid(cart.getcId(), cart.getcNum(), cart.getcCount());
        System.out.println(i);
    }

}
