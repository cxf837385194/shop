package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Goods;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context-mapper.xml"
        , "classpath:spring-druid.xml"
        , "classpath:spring-mybatis.xml"})
public class GoodsMapperTest {

    @Autowired
    private GoodsMapper goodsMapper;


    /**
     * 测试查询所有商品数据
     *   单元测试通过！！！
     *   swagger测试通过！！！
     */
    @Test
    public void testSelectAll(){
        List<Goods> goods = goodsMapper.selectAll();
        goods.stream().forEach(System.out::println);

    }

    /**
     *  测试根据商品Gid查询所有商品数据
     *   单元测试通过！！！
     *   swagger测试通过！！！
     */
    @Test
    public void testSelectGoodsById(){
        int gId = 1;
        Goods goods = goodsMapper.selectGoodsById(gId);
        System.out.println(goods);
    }

    /**
     *  测试根据商品类别Tid查询所有商品数据
     *   单元测试通过！！！
     *   swagger测试通过！！！
     */
    @Test
    public void testSelectGoodsByTid(){
        int tid=1;
        List<Goods> goods = goodsMapper.selectGoodsByTid(tid);
        goods.stream().forEach(System.out::println);
    }
}
