package cn.qf.shop.mapper;

import cn.qf.shop.pojo.entity.Address;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context-mapper.xml"
        , "classpath:spring-druid.xml"
        , "classpath:spring-mybatis.xml"})
public class AddressTest {
    @Resource
    private AddressMapper addressMapper ;

    @Test
    public void test() {
        System.out.println("addressMapper = " + addressMapper);
        Address address = new Address();

        address.setaName("xiaohong");
        address.setaTel("1888");
        address.setuId(1);
        address.setaDetail("广州千峰");
        addressMapper.insertAddressPost(address);
    }
}