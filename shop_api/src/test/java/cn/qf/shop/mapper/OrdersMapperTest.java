package cn.qf.shop.mapper;




import cn.qf.shop.pojo.entity.Cart;
import cn.qf.shop.pojo.entity.Orders;
import cn.qf.shop.utils.RandomUtils;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * 订单模块
 * mapper单元测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context-mapper.xml"
        , "classpath:spring-druid.xml"
        , "classpath:spring-mybatis.xml"})
public class OrdersMapperTest {

    //初始化Orders模块数据访问层对象(从spring容器中获取)
    @Resource
    private OrdersMapper ordersMapper ;

    /**
     * 1.测试查询所有订单数据
     *   单元测试通过！！！
     *   swagger测试通过！！！
     */
    @Test
    public void testSelectAllOrders(){

        //用户ID
        Integer uId = 1 ;

        //1.调用方法，进行数据库的数据访问
        List<Orders> ordersList = ordersMapper.selectAllOrders(uId);

        //2.遍历打印结果
        for (Orders orders : ordersList) {
            System.out.println(orders);
        }
    }


    /**
     * 2.测试根据订单编号查询订单信息
     *   单元测试通过！！！
     *   swagger测试通过！！！
     */
    @Test
    public void testSelectByOid(){
        Integer uId = 1 ;
        String oId = "15156165165151";
        Orders orders = ordersMapper.selectByOid(uId,oId);
        System.out.println(orders);
    }

    /**
     * 3.测试根据订单状态查询订单信息
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testSelectByOstate(){
        Integer uId = 1 ;
        Integer oState = 3;
        List<Orders> ordersList = ordersMapper.selectByOstate(uId,oState);
        for (Orders orders : ordersList) {
            System.out.println(orders);
        }
    }


    /**
     * 4.测试定点订单编号修改订单评价
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testUpdateAssessByOid(){
        String oId = "512708999" ;
        String oAssess = "测试订单评价" ;
        ordersMapper.updateAssessByOid(oId,oAssess);
    }


    /**
     * 4.测试添加订单
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testAddOrderByOid(){

        //1.用户ID
        Integer uId = 4 ;
        //2.地址ID
        Integer aId = 4 ;
        //3.订单编号

        String orderId = RandomUtils.createOrderId();
        //4.总金额
        BigDecimal count = new BigDecimal(8888) ;

        ordersMapper.addOrderByOid(uId,aId,orderId,count);
    }


    /**
     * 4.测试根据商品ID在购物车表中查询数据
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testSelectGoodsByGid(){

       //1.参数 商品ID
        Integer gId = 1 ;

        //2.调用dao层
        Cart cart = ordersMapper.selectGoodsByGid(gId);

        System.out.println(cart);
    }

    // upCartNumAndCartCountByOid


    /**
     * 5.测试定点商品ID在购物车表中更新商品的小计和数量
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testUpCartNumAndCartCountByOid(){

        //1.参数 商品ID
        Integer gId = 3 ;

        //2.参数 商品的小计
        BigDecimal count = new BigDecimal(250);

        //3.参数 商品的数量
        Integer num = 7 ;

        //4.调用dao层
        ordersMapper.upCartNumAndCartCountByOid(gId,count,num);
    }



    /**
     * 6.测试向购物车表中插入商品信息数据
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testAddGoodsToCart(){

        //1.参数 商品ID
        Integer gId = 4 ;

        //2.参数 商品的单价
        BigDecimal count = new BigDecimal(999);

        //3.参数 用户ID
        Integer uId = 4 ;

//        void addGoodsToCart(@Param("uId") Integer uId, @Param("gId")Integer gId, @Param("gPrice") BigDecimal gPrice);

        //4.调用dao层
        ordersMapper.addGoodsToCart(uId,gId,count);
    }


    /**
     * 6.测试向购物车表中插入商品信息数据
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testUpdatePayStatus(){

       String out_trade_no = "20200903174323" ;

        ordersMapper.updatePayStatus(out_trade_no);
    }



    /**
     * 6.测试向购物车表中插入商品信息数据
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testChageOstateTwo(){

        String out_trade_no = "20200903211757" ;

        ordersMapper.chageOstateTwo(out_trade_no);
    }


    /**
     * 6.测试向购物车表中插入商品信息数据
     *  单元测试通过！！！
     *  swagger测试通过！！！
     */
    @Test
    public void testChageOstateFour(){

        String out_trade_no = "20200903211757" ;

        ordersMapper.chageOstateFour(out_trade_no);
    }
}
