package cn.qf.shop.mapper;


import cn.qf.shop.pojo.entity.Type;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context-mapper.xml"
        , "classpath:spring-druid.xml"
        , "classpath:spring-mybatis.xml"})
public class TypeMapperTest {
    @Autowired
    private TypeMapper typeMapper;


    /**
     *  测试查询所有商品类别数据
     *   单元测试通过！！！
     *
     */

    @Test
    public void testSelectTypeAll(){

        List<Type> types = typeMapper.selectTypeAll();
        types.stream().forEach(System.out::println);

    }

    /**
     *  测试根据类别ID获取商品类别数据
     *   单元测试通过！！！
     *
     */
    @Test
    public void testSelectTypeById(){
        int tid=1;
        Type type = typeMapper.selectTypeById(tid);
        System.out.println(type);

    }
}
