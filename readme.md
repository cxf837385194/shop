# 商城说明

## 1. 数据库

### 1.1 用户表（user）

| 数据项       | 数据项别名 | 数据类型 | 长度   | 备注     |
| ------------ | ---------- | -------- | ------ | -------- |
| 用户id       | u_id       | bigint   | 20     | 用户主键 |
| 用户名       | u_name     | varchar  | 20     |          |
| 密码         | u_password | varchar  | 64     |          |
| 用户手机号   | u_phone    | varchar  | 11     |          |
| 邮箱         | u_email    | varchar  | 50     |          |
| 性别         | u_sex      | varchar  | 4      |          |
| 用户注册状态 | u_status   | int      | 11     |          |
| 用户码       | u_code     | varchar  | 64     |          |
| 用户余额     | u_balance  | decimal  | (12,2) |          |
| 用户等级     | u_levelint |          | 20     |          |

### 1.2 收货地址表 （address）

| 数据项         | 数据项别名   | 数据类型 | 长度 | 备注     |
| -------------- | ------------ | -------- | ---- | -------- |
| 地址id         | a_id         | bigint   | 20   | 地址主键 |
| 收货人名称     | a_name       | varchar  | 63   |          |
| 用户表的用户id | u_id         | bigint   | 20   |          |
| 省份           | a_province   | varchar  | 63   |          |
| 市             | a_city       | varchar  | 63   |          |
| 区县           | a_county     | varchar  | 63   |          |
| 详细收货地址   | a_detail     | varchar  | 127  |          |
| 手机号码       | a_tel        | varchar  | 20   |          |
| 是否默认地址   | a_is_default | tinyint  | 1    |          |
| 逻辑删除       | a_deleted    | tinyint  | 1    |          |

### 1.3 订单表 orders

| 数据项         | 数据项别名    | 数据类型  | 长度   | 备注 |
| -------------- | ------------- | --------- | ------ | ---- |
| 用户模块主键id | u_id          | bigint    | 20     |      |
| 地址模块主键   | a_id          | bigint    | 20     |      |
| 订单编号       | o_id          | varchar   | 64     |      |
| 订单创建时间   | o_create_time | timestamp |        |      |
| 订单修改时间   | o_update_time | timestamp |        |      |
| 订单总金额     | o_count       | decimal   | (12,2) |      |
| 订单状态       | o_state       | int       | 11     |      |
| 订单评价       | o_assess      | varchar   | 200    |      |

### 1.4 商品表(goods)

| 数据项              | 数据项别名 | 数据类型 | 长度   | 备注 |
| ------------------- | ---------- | -------- | ------ | ---- |
| 商品的唯一主键 ：() | g_id       | int      | 20     |      |
| 类别的主键id:  ()   | t_id       | int      | 20     |      |
| 商品的名称:()       | g_name     | varchar  | 50     |      |
| 商品的上市时间 :    | g_time     | date     |        |      |
| 商品图片的路径 : () | g_image    | varchar  | 100    |      |
| 商品的价格 :        | g_price    | decimal  | (12,2) |      |
| 商品的热门指数 : () | g_state    | int      | 11     |      |
| 商品的描述 : ()     | g_info     | varchar  | 200    |      |

### 1.5 购物车表(cart)

| 数据项             | 数据项别名 | 数据类型 | 长度 | 备注 |
| ------------------ | ---------- | -------- | ---- | ---- |
| 购物车唯一标识id   | c_id       | int      | 20   |      |
| 用户实体的主键属性 | u_id       | bigint   | 20   |      |
| 商品的唯一主键     | g_id       | int      | 20   |      |
| 购物车小计         | c_count    | decimal  |      |      |
| 购物车商品数量     | c_num      | int      | 20   |      |





### 1.6 订单项表(item)

| 数据项       | 数据项别名 | 数据类型 | 长度 | 备注 |
| ------------ | ---------- | -------- | ---- | ---- |
| 订单项的主键 | i_id       | int      | 20   |      |
| 订单项的小计 | i_count    | decimal  | 12,2 |      |
| 订单项的数量 | i_num      | int      | 20   |      |





88888














